CREATE TABLE admin (
  id SERIAL NOT NULL,
  identifiant VARCHAR(40),
  password VARCHAR(100),
  token text,
  expired_at timestamp with time zone NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE employe (
  id SERIAL NOT NULL,
  identifiant VARCHAR(40),
  password VARCHAR(100),
  token text,
  nom varchar(60),
  prenom varchar(60),
  expired_at DATE,
  PRIMARY KEY (id)
);

CREATE TABLE typeabonnement (
  id  SERIAL NOT NULL, 
  nom varchar(50), 
  PRIMARY KEY (id));

  
CREATE TABLE Prestation (
  id   SERIAL NOT NULL, 
  nom  varchar(50), 
  fixe int4, 
  PRIMARY KEY (id));
CREATE TABLE Lieu (
  id       SERIAL NOT NULL, 
  nom      varchar(50), 
  typelieu int4 NOT NULL, 
  maxplace int4, 
  PRIMARY KEY (id));
CREATE TABLE typelieu (
  id          SERIAL NOT NULL, 
  nomtypelieu varchar(40), 
  PRIMARY KEY (id));
CREATE TABLE typetarif (
  id       SERIAL NOT NULL, 
  nomtarif int4, 
  PRIMARY KEY (id));
CREATE TABLE elementprestation (
  id         SERIAL NOT NULL, 
  prestation int4 NOT NULL, 
  nomelement varchar(40), 
  typetarif  int4 NOT NULL, 
  prix       double precision, 
  typeabn    int4 NOT NULL, 
  place      int4, 
  PRIMARY KEY (id));
CREATE TABLE devis (
  id   SERIAL NOT NULL, 
  ref  varchar(20), 
  daty date, 
  PRIMARY KEY (id));
CREATE TABLE elementdevis (
  id               SERIAL NOT NULL, 
  devis            int4 NOT NULL, 
  prestation       int4 NOT NULL, 
  valeurprestation int4 NOT NULL, 
  prix             double precision, 
  quantite integer not null,
  duree double precision,
  PRIMARY KEY (id));

    insert into admin(identifiant,password) values ('admin@gmail.com',md5('admin123'));
    insert into employe(identifiant,password,nom,prenom) values ('randria@gmail.com',md5('emp123'),'Randria','Boulevard');
ALTER TABLE Lieu ADD CONSTRAINT FKLieu842190 FOREIGN KEY (typelieu) REFERENCES typelieu (id);
ALTER TABLE elementprestation ADD CONSTRAINT FKelementpre528226 FOREIGN KEY (prestation) REFERENCES Prestation (id);
ALTER TABLE elementprestation ADD CONSTRAINT FKelementpre517830 FOREIGN KEY (typetarif) REFERENCES typetarif (id);
ALTER TABLE elementprestation ADD CONSTRAINT FKelementpre778162 FOREIGN KEY (typeabn) REFERENCES typeabonnement (id);
ALTER TABLE elementdevis ADD CONSTRAINT FKelementdev41822 FOREIGN KEY (devis) REFERENCES devis (id);
ALTER TABLE elementdevis ADD CONSTRAINT FKelementdev424581 FOREIGN KEY (prestation) REFERENCES Prestation (id);
ALTER TABLE elementdevis ADD CONSTRAINT FKelementdev311236 FOREIGN KEY (valeurprestation) REFERENCES elementprestation (id);

CREATE VIEW V_prestation AS
SELECT
  p.id AS prestation_id,
  p.nom AS prestation_nom,
  p.fixe AS prestation_fixe,
  ep.id AS elementprestation_id,
  ep.nomelement AS elementprestation_nomelement,
  ep.typetarif AS elementprestation_typetarif,
  ep.prix AS elementprestation_prix,
  ep.typeabn AS elementprestation_typeabn,
  ep.place AS elementprestation_place
FROM
  Prestation p
JOIN
  elementprestation ep ON p.id = ep.prestation;

CREATE VIEW V_prestation_abonnement AS
SELECT
  vp.prestation_id,
  vp.prestation_nom,
  vp.prestation_fixe,
  vp.elementprestation_id,
  vp.elementprestation_nomelement,
  vp.elementprestation_typetarif,
  vp.elementprestation_prix,
  vp.elementprestation_typeabn,
  vp.elementprestation_place,
  ta.nom AS typeabonnement_nom,
  tt.nomtarif
FROM
  V_prestation vp
JOIN
  typeabonnement ta ON vp.elementprestation_typeabn = ta.id
JOIN
  typetarif tt ON vp.elementprestation_typetarif = tt.id;



CREATE OR REPLACE VIEW Lieux
as
select * from elementprestation where prestation=4

CREATE OR REPLACE VIEW Artists
as
select * from elementprestation where prestation=1


CREATE OR REPLACE VIEW Sonoristations
as
select * from elementprestation where prestation=2

CREATE OR REPLACE VIEW Logistiques
as
select * from elementprestation where prestation=3

CREATE TABLE evenement(
  id serial primary key,
  nom varchar(40)
);

CREATE TABLE CategoriePlace(
  id serial primary key,
  nom varchar(40)
);

CREATE TABLE NombreCategoriePlace(
  id serial primary key,
  idlieu integer REFERENCES elementprestation(id),
  idcategorieplace integer REFERENCES categorieplace(id),
  nb integer
);

create or replace view V_nbplace as select coalesce(sum(nb),0)::integer as nombretotal,lieux.id from nombrecategorieplace right join lieux on lieux.id=idlieu group by lieux.id;


CREATE TABLE prixplaceDevis(
  id serial primary key,
  devis integer REFERENCES devis(id),
  categorieplace integer references categorieplace(id),
  prix double precision
);

CREATE OR REPLACE VIEW V_devis_evenement AS
SELECT
  d.id AS id,
  d.ref AS devis_ref,
  d.daty AS devis_daty,
  e.id AS evenement_id,
  e.nom AS evenement_nom,
  d.nomevenement
FROM
  devis d
JOIN
  evenement e ON d.evenement = e.id;

  
  CREATE OR REPLACE VIEW V_artiste_spectacle
  as
 select 
 elementdevis.id as id,
 devis ,
  elementdevis.prestation ,
  valeurprestation ,
   elementdevis.prix  ,
  quantite ,
  duree,
  nomelement as nomartiste,
  photo
  from elementdevis join artists on artists.id=valeurprestation;

  CREATE OR REPLACE VIEW V_prixdevis
  as
  select
  prixplacedevis.*,
  categorieplace.nom
  from
  prixplacedevis
  join 
  categorieplace
  on 
   prixplacedevis.categorieplace=categorieplace.id;

   CREATE OR REPLACE VIEW V_prixdevis
  as
  select
  prixplacedevis.*,
  categorieplace.nom
  from
  prixplacedevis
  join 
  categorieplace
  on 
   prixplacedevis.categorieplace=categorieplace.id;

     CREATE OR REPLACE VIEW V_lieux_spectacle
  as

 select 
 elementdevis.id as id,
 devis ,
  elementdevis.prestation ,
  valeurprestation ,
   elementdevis.prix  ,
  quantite ,
  duree,
  nomelement as nomartiste,
  photo
  from elementdevis join lieux on lieux.id=valeurprestation;

  CREATE OR REPLACE VIEW V_elementdevis_prestation AS
SELECT
  ed.id AS id,
  ed.devis ,
  p.id AS prestation_id,
  p.nom AS prestation,
  ep.nomelement,
  ed.valeurprestation as elementprestation_id,
  ed.duree,
  ed.prix,
  ed.quantite
FROM
  elementdevis ed
JOIN
  prestation p ON ed.prestation = p.id
JOIN
  elementprestation ep ON  ed.valeurprestation = ep.id;

CREATE VIEW V_elementdevis_prestation_devis
as
select 
devis.id as devis_id,
devis.nomevenement,
V_elementdevis_prestation.*
from
V_elementdevis_prestation
join devis on
V_elementdevis_prestation.devis=devis.id;


CREATE TABLE taxe(
  id serial primary key,
  minvalue double precision,
  maxvalue double precision,
  pourcentage double precision
);

CREATE TABLE placevendu(
  id serial primary key,
  categorieplace integer references categorieplace(id),
  nombrevendu integer,
  devis integer references devis(id)
);
