CREATE TABLE Magasin (id SERIAL NOT NULL, nom VARCHAR(50), PRIMARY KEY (id));

CREATE TABLE AdminMagasin (
  id SERIAL NOT NULL,
  identifiant VARCHAR(40),
  password VARCHAR(100),
  token text,
  expired_at timestamp with time zone NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE admindepot (
  id SERIAL NOT NULL,
  identifiant VARCHAR(40),
  password VARCHAR(100),
  token text,
  expired_at DATE,
  PRIMARY KEY (id)
);

  CREATE sequence  depot_seq;
  CREATE sequence  produit_seq;
  CREATE sequence  mouvementmagasin_seq;
CREATE TABLE produit (
  id SERIAL NOT NULL,
  REF VARCHAR(20),
  descriptionproduit text,
  PRIMARY KEY (id)
);

CREATE TABLE Depot (
  id SERIAL NOT NULL,
  REF VARCHAR(20),
  nom VARCHAR(40),
  idmagasin integer NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE MouvementMagasin (
  id SERIAL NOT NULL,
  REF VARCHAR(20),
  motif VARCHAR(255),
  daty DATE DEFAULT CURRENT_TIMESTAMP,
  entree integer DEFAULT 0 CHECK(entree >= 0),
  sortie integer DEFAULT 0 CHECK(sortie >= 0),
  idmagasin integer NOT NULL,
  produit integer NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE MouvementDepot (
  id SERIAL NOT NULL,
  REF VARCHAR(20),
  daty DATE,
  entree integer,
  sortie integer,
  motif VARCHAR(255),
  iddepot integer NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE histotransfertmagasin (
  id SERIAL NOT NULL,
  idmagasin integer NOT NULL,
  idMouvementMagasin integer NOT NULL,
  Depotid integer NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE histotransfertdepot (
  id SERIAL NOT NULL,
  sender integer NOT NULL references Magasin(id),
  receiver integer NOT NULL references depot(id),
  PRIMARY KEY (id)
);

ALTER TABLE
  Depot
ADD
  CONSTRAINT FKDepot408363 FOREIGN KEY (idmagasin) REFERENCES Magasin (id);

ALTER TABLE
  MouvementMagasin
ADD
  CONSTRAINT FKMouvementM712912 FOREIGN KEY (idmagasin) REFERENCES Magasin (id);

ALTER TABLE
  MouvementMagasin
ADD
  CONSTRAINT FKMouvementM279101 FOREIGN KEY (produit) REFERENCES produit (id);

ALTER TABLE
  MouvementDepot
ADD
  CONSTRAINT FKMouvementD234876 FOREIGN KEY (iddepot) REFERENCES Depot (id);

ALTER TABLE
  histotransfertmagasin
ADD
  CONSTRAINT FKhistotrans327307 FOREIGN KEY (idmagasin) REFERENCES Magasin (id);

ALTER TABLE
  histotransfertmagasin
ADD
  CONSTRAINT FKhistotrans79335 FOREIGN KEY (idMouvementMagasin) REFERENCES MouvementMagasin (id);

ALTER TABLE
  histotransfertmagasin
ADD
  CONSTRAINT FKhistotrans901473 FOREIGN KEY (Depotid) REFERENCES Depot (id);

ALTER TABLE
  histotransfertdepot
ADD
  CONSTRAINT FKhistotrans884370 FOREIGN KEY (sender) REFERENCES Depot (id);

ALTER TABLE
  histotransfertdepot
ADD
  CONSTRAINT FKhistotrans358792 FOREIGN KEY (receiver) REFERENCES Depot (id);


  insert into AdminMagasin(identifiant,password) values ('mikolo@gmail.com',md5('mikolo123'));
  insert into admindepot(identifiant,password) values ('depottana@gmail.com',md5('tana123'));
  insert into admindepot(identifiant,password) values ('pdv1@gmail.com',md5('pdv123'));
  insert into admindepot(identifiant,password) values ('pdv2@gmail.com',md5('pdv123!'));

ALTER TABLE public.depot ALTER COLUMN "ref" SET DEFAULT (('DP'::text || nextval('depot_seq'::regclass)));

CREATE TABLE Diskdur (
  id SERIAL NOT NULL,
  capacite int4,
  typedisk int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE ecran (
  id SERIAL NOT NULL,
  resolutionX int4,
  resulotionY int4,
  PRIMARY KEY (id)
);

CREATE TABLE Marque (id SERIAL NOT NULL, nom VARCHAR(50), PRIMARY KEY (id));

CREATE TABLE Modele (
  id SERIAL NOT NULL,
  nommodele VARCHAR(20),
  idmarque int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE processeur (
  id SERIAL NOT NULL,
  reference VARCHAR(20),
  marque int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE  TABLE produit (
  id SERIAL NOT NULL,
  REF VARCHAR(20),
  description int4,
  modele int4 NOT NULL,
  ram int4 NOT NULL,
  ecran int4 NOT NULL,
  diskdur int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE ram (
  id SERIAL NOT NULL,
  capacite int4,
  description VARCHAR(255),
  type int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE typedisk (id SERIAL NOT NULL, nom VARCHAR(50), PRIMARY KEY (id));

CREATE TABLE typeram (id SERIAL NOT NULL, nom VARCHAR(50), PRIMARY KEY (id));

ALTER TABLE
  Diskdur
ADD
  CONSTRAINT FKDisk555903 FOREIGN KEY (typedisk) REFERENCES typedisk (id);

ALTER TABLE
  ram
ADD
  CONSTRAINT FKram381434 FOREIGN KEY (type) REFERENCES typeram (id);

ALTER TABLE
  processeur
ADD
  CONSTRAINT FKprocesseur431194 FOREIGN KEY (marque) REFERENCES Marque (id);

ALTER TABLE
  Modele
ADD
  CONSTRAINT FKModele820001 FOREIGN KEY (idmarque) REFERENCES Marque (id);

ALTER TABLE
  produit
ADD
  CONSTRAINT FKproduit766079 FOREIGN KEY (modele) REFERENCES Modele (id);

ALTER TABLE
  produit
ADD
  CONSTRAINT FKproduit245493 FOREIGN KEY (ram) REFERENCES ram (id);

ALTER TABLE
  produit
ADD
  CONSTRAINT FKproduit795438 FOREIGN KEY (ecran) REFERENCES ecran (id);

ALTER TABLE
  produit
ADD
  CONSTRAINT FKproduit256786 FOREIGN KEY (diskdur) REFERENCES Diskdur (id);


  INSERT INTO ram (capacite, description, type) VALUES
  (8, '8GB RAM', 1),
  (16, '16GB RAM', 1),
  (32, '32GB RAM', 1),
  (256, '256GB RAM', 2),
  (512, '512GB RAM', 2),
  (1024, '1TB RAM', 2);



  INSERT INTO marque (nom) VALUES
  ('DELL'),
  ('HP'),
  ('ASUS');

  INSERT INTO modele (nommodele, idmarque) VALUES
  ('Dell Latitude', 1),
  ('Dell G-15', 1),
  ('HP PRO book', 2),
  ('HP Pro Book g9', 2),
  ('Asus ROG', 3);

  CREATE TABLE capacite(
    id serial not null,
    valeur integer not null,
    primary key(id)
  );

  ALTER TABLE public.ecran ADD description varchar NULL DEFAULT 'Ecran';
ALTER TABLE public.produit ADD prix integer NOT NULL DEFAULT 0;

ALTER TABLE public.mouvementmagasin ADD price integer NOT NULL DEFAULT 0;

CREATE TABLE histotransfertmagasin (
  id SERIAL NOT NULL,
  idmagasin integer NOT NULL,
  idMouvementdepot integer NOT NULL,
  Depotid integer NOT NULL,
  PRIMARY KEY (id)
);




CREATE OR REPLACE VIEW Viewentree
as
select produit.id,
sum(detailsmouvement.entree)::int as entree
from produit
join
detailsmouvement
on 
detailsmouvement.produit=produit.id
group by produit.id;
CREATE OR REPLACE VIEW Viewsortie
as
select produit.id,
sum(detailsmouvement.sortie)::int as sortie
from produit
join
detailsmouvement
on 
detailsmouvement.produit=produit.id
group by produit.id;



CREATE OR REPLACE VIEW ProduitDisponible
as

select produit.*,
(coalesce(entree,0)-coalesce(sortie,0))::int as disponible
from produit
left join
Viewentree
on 
Viewentree.id=produit.id
left join
Viewsortie
on 
Viewsortie.id=produit.id;


CREATE TABLE DetailsMouvement(
  id serial primary key,
  idMouvementMagasin integer REFERENCES mouvementmagasin(id),
  entree integer not null default 0,
  sortie integer not null default 0
);

CREATE TABLE DetailsMouvementDepot(
  id serial primary key,
  iddepot integer REFERENCES mouvementdepot(id),
  entree integer not null default 0,
  sortie integer not null default 0
);


ALTER TABLE public.detailsmouvement ADD produit integer NULL;
ALTER TABLE public.detailsmouvement ADD CONSTRAINT detailsmouvement_fk FOREIGN KEY (produit) REFERENCES public.produit(id);

ALTER TABLE public.mouvementdepot DROP COLUMN entree;
ALTER TABLE public.mouvementdepot DROP COLUMN sortie;
ALTER TABLE public.detailsmouvementdepot ADD produit integer NULL;
ALTER TABLE public.detailsmouvementdepot ADD CONSTRAINT detailsmouvementdepot_fk FOREIGN KEY (produit) REFERENCES public.produit(id);

CREATE OR REPLACE VIEW ViewentreeDepot
as
select produit.id,
sum(detailsmouvementdepot.entree)::int as entree
from produit
join
detailsmouvementdepot
on 
detailsmouvementdepot.produit=produit.id
group by produit.id;
CREATE OR REPLACE VIEW ViewsortieDepot
as
select produit.id,
sum(detailsmouvementdepot.sortie)::int as sortie
from produit
join
detailsmouvementdepot
on 
detailsmouvementdepot.produit=produit.id
group by produit.id;

CREATE OR REPLACE VIEW ProduitDisponibleDepot
as

select produit.*,
(coalesce(entree,0)-coalesce(sortie,0))::int as disponible
from produit
left join
Viewentreedepot
on 
Viewentreedepot.id=produit.id
left join
Viewsortiedepot
on 
Viewsortiedepot.id=produit.id;

ALTER TABLE public.histotransfertdepot RENAME COLUMN sender TO idmagasin;
ALTER TABLE public.histotransfertdepot RENAME COLUMN receiver TO depotid;
ALTER TABLE public.histotransfertdepot ADD idmouvementdepot integer NULL;
ALTER TABLE public.histotransfertdepot ADD CONSTRAINT histotransfertdepot_fk FOREIGN KEY (idmouvementdepot) REFERENCES public.mouvementdepot(id);


CREATE  or replace view TransactPvMouvement
as
SELECT * from mouvementmagasin where id in (select idmouvementmagasin from histotransfertmagasin);


CREATE OR REPLACE VIEW TransactPvMouvement_Details
as
SELECT detailsmouvement.*,ref,idmagasin from detailsmouvement
join TransactPvMouvement
on 
TransactPvMouvement.id=detailsmouvement.idmouvementmagasin;

CREATE OR REPLACE VIEW Transact
as

SELECT sum(entree)::int as entree,sum(sortie) as sortie,idmagasin from
TransactPvMouvement_Details
group by idmagasin;


CREATE  or replace view TransactToMg
as
SELECT * from mouvementdepot where id in (select idmouvementdepot from histotransfertdepot);

CREATE OR REPLACE VIEW TransactToMg_details
as
SELECT detailsmouvementdepot.*,ref,detailsmouvementdepot.iddepot as idMouvementdepot from detailsmouvementdepot
join TransactToMg
on 
TransactToMg.id=detailsmouvementdepot.iddepot;

CREATE OR REPLACE VIEW TransactDepot
as

SELECT sum(entree)::int as entree,sum(sortie) as sortie,iddepot from
TransactToMg_details
group by iddepot;



CREATE VIEW Magasin_mandray
as
select * from histotransfertmagasin where reference is  not null;


CREATE VIEW Magasin_mandray_mouvement_depot
as
select 
idmagasin ,
 idmouvementmagasin ,
 depotid ,
 reference,
 ref,
  daty,
mouvementdepot.id as idmvdepot
 from Magasin_mandray
join mouvementdepot
on mouvementdepot.ref=reference;


select 
idmagasin ,
 detailsmouvement.idmouvementmagasin ,
 depotid ,
 reference
idmvdepot ,
 DetailsMouvementdepot.entree  as nalefa_tany_magasin,
 DetailsMouvementdepot.sortie as voaray_magasin,
  detailsmouvement.*
 from Magasin_mandray_mouvement_depot
join DetailsMouvementdepot
on 
detailsmouvementdepot.iddepot = Magasin_mandray_mouvement_depot.idmvdepot
join detailsmouvement
on 
detailsmouvement.idmouvementmagasin=Magasin_mandray_mouvement_depot.idmouvementmagasin



CREATE TABLE Pourcentage(
  id serial PRIMARY KEY,
  Pourcentage double precision not null
);

CREATE OR REPLACE FUNCTION getPourcentage()
  RETURNS double precision
AS $$
DECLARE
  pourcentage double precision;
BEGIN
  SELECT Pourcentage.pourcentage INTO pourcentage FROM Pourcentage LIMIT 1;
  RETURN pourcentage;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE View V_vente
as
select 
vente.*,
produit.prix,
produit.description,
produit.ref as referenceproduit,
produit.id,
produit.prixvente
from vente
join produit
on produit.id=vente.produit;



CREATE TABLE Commission(
  id serial primary key,
  total_min double precision,
  total_max double precision CHECK(total_max>total_min),
  commission double precision
);