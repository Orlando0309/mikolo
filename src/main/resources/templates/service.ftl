/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ${packageservice};

import com.strawberry.bloom.BerryTree;
import ${packageclass}.${model?cap_first};
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class Service${model?cap_first} extends BerryTree<${model?cap_first}> {
    
    public Service${model?cap_first}() throws Exception {
        super(${model?cap_first}.class);
    }
    
}
