<#assign tableMappings = generator.Columns(tableName)>
<#assign hasDateImport = generator.importget()>
<#assign className = generator.className(mod)>
package ${packages};
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
<#if loads>
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
</#if>
<#if hasDateImport?has_content>
import java.util.Date;
</#if>


@TableName("${tableName}")
public class ${className} {
    <#list tableMappings?keys as columnName>
<#assign map=tableMappings[columnName]>
<#assign teste=map.isprimary >
 <#if teste>
    @Id
    </#if>
<#if teste>
    @ColumnName(insertable = false, hasdefault = true)
<#else>
    @ColumnName
    </#if>
    
    <#if loads>
        <#if !teste>
            @FieldLoad(type = "<#if map.isforeign>arrayOption<#else>${map.loadtype}</#if>")
        </#if>
     </#if>
    ${map.javatype} ${columnName};

    </#list>

    <#list tableMappings?keys as columnName>
<#assign map=tableMappings[columnName]>
    public ${map.javatype} get${columnName?cap_first}() {
        return ${columnName};
    }

    public void set${columnName?cap_first}(${map.javatype} ${columnName}) {
        this.${columnName} = ${columnName};
    }
    </#list>
}
