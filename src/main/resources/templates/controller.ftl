package ${controller};

import ${packagesearch}.${model?cap_first}Search;
import ${packageclass}.${model?cap_first};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.${tokenable};
import ${packageservice}.Service${model?cap_first};

/**
 *
 * @author andri
 */
@RequestMapping("/${url}")
@Component
@RestController
@CrossOrigin(value = "*")
public class ${model?cap_first}Controller extends ScaffoldController<${model?cap_first}, Service${model?cap_first}, ${model?cap_first}Search, ${tokenable}> {
    
    @Autowired
    public ${model?cap_first}Controller(Service${model?cap_first} service, ${tokenable} tokenable, ResourceLoader resourceLoader) {
        super(service, ${model?cap_first}.class, new ${model?cap_first}Search(${model?cap_first}.class), tokenable, resourceLoader);
    }
    
}
