<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${titre}</title>

    <style type="text/css">
        body{
            font-family:Arial, Helvetica, sans-serif;
            background-color: #f2f2f2;
        }
      table {
        border-collapse: collapse;
        width: 100%;
        background-color: #f2f2f2;
        border-radius: 10px;
        box-shadow: 7px 7px 10px #d9d9d9, -7px -7px 10px #ffffff;
      }

      th,
      td {
        border: 1px solid #dee2e6;
        padding: 8px;
        text-align: left;
      }

      th {
        background-color: #f8f9fa;
        border-radius: 10px;
        box-shadow: 5px 5px 8px #d9d9d9, -5px -5px 8px #ffffff;
      }

      .table-striped tbody tr:nth-of-type(odd) {
        background-color: #f2f2f2;
      }

      .table-hover tbody tr:hover {
        background-color: #e6f2ff;
      }

      .table-bordered th,
      .table-bordered td {
        border: 1px solid #dee2e6;
      }

      .table-bordered {
        border: 1px solid #dee2e6;
        border-radius: 10px;
        box-shadow: 7px 7px 10px #d9d9d9, -7px -7px 10px #ffffff;
      }

      .table-bordered tbody tr:nth-of-type(odd) {
        background-color: #f2f2f2;
      }
    </style>
  </head>
  <body>
    <div style="background-color:#f2f2f2;border-radius:10px;box-shadow:7px 7px 10px #d9d9d9, -7px -7px 10px #ffffff; padding: 20px;">
      <h1 style="font-size: 48px; text-align: center; color: #ffffff; text-shadow: 2px 2px 4px #000000; border-radius: 10px; box-shadow: 7px 7px 10px #d9d9d9, -7px -7px 10px #ffffff; padding: 20px; background-color: #4d4dff;">${titre}</h1>
      ${table}
    </div>
  </body>
</html>