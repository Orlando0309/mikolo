<#assign  Date=affiche.extractDate(daty)>
<#assign  Heure=affiche.extractTime(daty)>
<#assign  Lieu=affiche.getLieux()>
<#assign  prices=affiche.listeprix()>
<!DOCTYPE html>
<html>
<head>
  <title>Affiche du Spectacle</title>
  <style>
  /* Ajoutez vos styles dédiés pour l'affiche ici */
/* Styles pour l'affiche du spectacle */
body {
  font-family: Arial, sans-serif;
  background-color: #f2f2f2;
}

.poster-container {
  width: 600px;
  margin: 20px auto;
  background-color: #fff;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);
  padding: 20px;
  text-align: center;
}

.poster-container img {
  width: 100%;
  height: auto;
  margin-bottom: 20px;
}

.poster-container h1 {
  font-size: 32px;
  margin-bottom: 10px;
  color: #333;
  text-transform: uppercase;
}

.poster-container p {
  font-size: 18px;
  margin-bottom: 10px;
  color: #555;
}

.poster-container .artist-image {
  display: inline-block;
  width: 80px;
  height: 80px;
  border-radius: 50%;
  margin-right: 10px;
}

.poster-container .artist-name {
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 5px;
}

.poster-container .price {
  font-size: 24px;
  font-weight: bold;
  color: #FF5722;
}

  </style>
</head>
<body>
  <div class="poster-container">
    <!-- <img src="path_to_poster_image.jpg" alt="Affiche du Spectacle"> -->
    <h1>${titre}</h1>
    <p>Date: ${Date}</p>
    <p>Heure: ${Heure}</p>
    <p>Lieu: ${Lieu.nomartiste}</p>
    <#if Lieu.photo?has_content>
    <img src="${Lieu.photo}" alt="Photo du Lieu">
    </#if>
    <p>Nom des Artistes:</p>
    <div class="artists">
      <#list artists as artist>
      <div class="artist">
        <#if artist.photo?has_content>
          <img src="${artist.photo}" alt="Photo Artiste 1" class="artist-image">
        </#if>
        <span>${artist.nomartiste}</span>
      </div>
      </#list>

    </div>
    <p>Prix des Places: 
      <ul>
        <#list prices as price>
          <li>${price.nom}:<span class="price">${price.prix}</span></p></li>
        </#list>
      </ul>
      
  </div>
</body>
</html>
