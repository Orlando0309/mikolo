package ${packageclass};

import modele.view.${model?cap_first};
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ${model?cap_first}Search  extends SearchTools<${model?cap_first}> {
    
    public ${model?cap_first}Search(Class<${model?cap_first}> clT) {
        super(clT);
    }
    
}