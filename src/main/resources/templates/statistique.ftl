<!DOCTYPE html>
<html>
<head>
    <title>Statistique List</title>
    <style type="text/css">
        body{
            font-family:Arial, Helvetica, sans-serif;
        }
      table {
        border-collapse: collapse;
        width: 100%;
      }

      th,
      td {
        border: 1px solid #dee2e6;
        padding: 8px;
        text-align: left;
      }

      th {
        background-color: #f8f9fa;
      }

      .table-striped tbody tr:nth-of-type(odd) {
        background-color: #f8f9fa;
      }

      .table-hover tbody tr:hover {
        background-color: #f1f5fe;
      }

      .table-bordered th,
      .table-bordered td {
        border: 1px solid #dee2e6;
      }

      .table-bordered {
        border: 1px solid #dee2e6;
      }

      .table-bordered tbody tr:nth-of-type(odd) {
        background-color: #f8f9fa;
      }
    </style>
</head>
<body>
    <h1>Statistique List</h1>
    <table>
        <thead>
            <tr>
                <th>Mois</th>
                <th>Vente</th>
            </tr>
        </thead>
        <tbody>
            <#list statistiques as statistique>
                <tr>
                    <td>${statistique.mois}</td>
                    <td>${statistique.vente}</td>
                </tr>
            </#list>
        </tbody>
    </table>
</body>
</html>
