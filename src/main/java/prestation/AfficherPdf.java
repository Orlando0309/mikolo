/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import modele.view.Devis;
import modele.view.Lieux;
import modele.view.V_artiste_spectacle;
import modele.view.V_lieux_spectacle;
import modele.view.V_prixdevis;
import service.ServiceLieux;
import service.ServiceV_artiste_spectacle;
import service.ServiceV_lieux_spectacle;
import service.ServiceV_prixdevis;

/**
 *
 * @author andri
 */
public class AfficherPdf {

    Devis devis;

    public String extractDate(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(new Date(timestamp.getTime()));
    }

    public String extractTime(Timestamp timestamp) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        return timeFormat.format(new Date(timestamp.getTime()));
    }

    public V_artiste_spectacle[] liste() throws Exception {
//        System.out.println("");
        ServiceV_artiste_spectacle ser = new ServiceV_artiste_spectacle();
        V_artiste_spectacle spe = new V_artiste_spectacle();
        spe.setDevis(devis.getId());
        return ser.filter(spe);
    }
    public V_prixdevis[] listeprix() throws Exception{
        ServiceV_prixdevis ser=new ServiceV_prixdevis();
        V_prixdevis spe = new V_prixdevis();
        spe.setDevis(devis.getId());
        return ser.filter(spe);
    }

    public V_lieux_spectacle getLieux() throws Exception {
        ServiceV_lieux_spectacle ser=new ServiceV_lieux_spectacle();
        V_lieux_spectacle spe=new V_lieux_spectacle();
        spe.setDevis(devis.getId());
        return ser.getRow(spe);
    }

    public Devis getDevis() {
        return devis;
    }

    public void setDevis(Devis devis) {
        this.devis = devis;
    }

}
