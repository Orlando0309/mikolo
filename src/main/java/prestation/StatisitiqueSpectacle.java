/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import java.util.ArrayList;
import java.util.List;
import modele.view.Devis;
import service.ServiceDevis;

/**
 *
 * @author andri
 */
public class StatisitiqueSpectacle {

    Devis[] devis;
    List<Benefice> benefice=new ArrayList<>();

    public void init() throws Exception {
        ServiceDevis se = new ServiceDevis();
        devis = se.list();
        for (Devis i : devis) {
            Benefice ben = new Benefice();
            ben.initialise(i);
            ben.getTax();
            ben.calculTax();
            ben.calculTax();
            ben.beneficeNet();
            ben.setDevis(i);
            benefice.add(ben);
        }
    }

    public Devis[] getDevis() {
        return devis;
    }

    public void setDevis(Devis[] devis) {
        this.devis = devis;
    }

    public List<Benefice> getBenefice() {
        return benefice;
    }

    public void setBenefice(List<Benefice> benefice) {
        this.benefice = benefice;
    }

}
