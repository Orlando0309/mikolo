/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import modele.view.Devis;
import modele.view.Elementdevis;
import modele.view.PrixplaceDevis;
import service.ServiceDevis;
import service.ServiceElementdevis;
import service.ServicePrixplaceDevis;

/**
 *
 * @author andri
 */
public class DevisHelper {
    Devis devis;
    Elementdevis[] elementdevis;
    PrixplaceDevis[] prixplacedevis;

    
    public void save() throws Exception{
        ServiceDevis sdevis=new ServiceDevis();
        sdevis.save(devis);
        Devis dev=sdevis.getRow(devis);
        Integer id=dev.getId();
        ServiceElementdevis seld=new ServiceElementdevis();
        ServicePrixplaceDevis spd=new ServicePrixplaceDevis();
        for(Elementdevis i: elementdevis){
            i.setDevis(id);
            seld.save(i);
        }
        for(PrixplaceDevis i:prixplacedevis){
            i.setDevis(id);
            spd.save(i);
        }
    }
    public Devis getDevis() {
        return devis;
    }

    public void setDevis(Devis devis) {
        this.devis = devis;
    }

    public Elementdevis[] getElementdevis() {
        return elementdevis;
    }

    public void setElementdevis(Elementdevis[] elementdevis) {
        this.elementdevis = elementdevis;
    }

    public PrixplaceDevis[] getPrixplacedevis() {
        return prixplacedevis;
    }

    public void setPrixplacedevis(PrixplaceDevis[] prixplacedevis) {
        this.prixplacedevis = prixplacedevis;
    }
    
}
