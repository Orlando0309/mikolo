/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import doframe.own.exception.ReflectException;
import java.util.Arrays;
import java.util.List;
import modele.view.V_recette;
import scaffold.chart.ChartData;
import scaffold.chart.Datasets;
import terminal.Arithmetic;

/**
 *
 * @author andri
 */
public class RecetteHelper {
    V_recette[] liste;
    Double total;

    public RecetteHelper(V_recette[] liste) {
        this.liste = liste;
    }
    
    public void calculate() throws ReflectException{
        Arithmetic<V_recette> recette=new Arithmetic<>();
        recette.setObject(liste);
        total=recette.sum("recette");
    }
    public V_recette[] get() throws ReflectException{
        calculate();
        for(V_recette i:liste){
            i.setRecette((i.getRecette()/total)*100);
        }
        return liste;
    }
    public ChartData toDatasets() throws ReflectException{
        V_recette[] l=liste;
        List<V_recette> lis=Arrays.asList(l);
        String[] labels=lis.stream().map(V_recette::getNom).toArray(String[]::new);
        Double[] recettes=lis.stream().map(V_recette::getRecette).toArray(Double[]::new);
      
        ChartData c=new ChartData(labels,new Datasets("Data", recettes, new String[]{"#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#66CC99",
                  "#9966FF",
                  "#FF9999"}, "#FFF", 1));
        return c;
    }
}
