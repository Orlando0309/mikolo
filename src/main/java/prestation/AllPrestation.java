/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import modele.view.Prestation;
import service.ServicePrestation;

/**
 *
 * @author andri
 */
public class AllPrestation {
    Prestation[] fixe;
    Prestation[] nonfixe;

    public void init() throws Exception{
        ServicePrestation prestation=new ServicePrestation();
        setNonfixe(prestation.get(0));
        setFixe(prestation.get(1));
    }
    public Prestation[] getFixe() {
        return fixe;
    }

    public void setFixe(Prestation[] fixe) {
        this.fixe = fixe;
    }

    public Prestation[] getNonfixe() {
        return nonfixe;
    }

    public void setNonfixe(Prestation[] nonfixe) {
        this.nonfixe = nonfixe;
    }
    
}
