/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import java.util.Objects;
import modele.view.Elementdevis;
import modele.view.Nombrecategorieplace;
import modele.view.Placevendu;
import modele.view.PrixplaceDevis;
import service.ServiceNombrecategorieplace;

/**
 *
 * @author andri
 */
public class Recette extends NbPlaceHelper {
    Nombrecategorieplace[] listecategorie;
    Double recette;
    Double depense;
    Elementdevis[] element;
    Placevendu[] placevendu;
    Double realrecette;
    Double benefice;

    public Double getRealrecette() {
        return realrecette;
    }

    public void setRealrecette(Double realrecette) {
        this.realrecette = realrecette;
    }

    public Double getBenefice() {
        return benefice;
    }

    public void setBenefice(Double benefice) {
        this.benefice = benefice;
    }
    
    public void calculbenefice(){
        benefice=realrecette-depense;
    }
    public void calculerRealRecette(){
        Double retour=0d;
        for(Placevendu i:placevendu){
        Double temp=0d;
        for(PrixplaceDevis j:getPriceplace()){
            if(Objects.equals(j.getCategorieplace(), i.getCategorieplace())){
                temp=j.getPrix()*i.getNombrevendu();
                break;
            }
        }
        retour+=temp;
    }
    realrecette=retour;
    }
    

    public Placevendu[] getPlacevendu() {
        return placevendu;
    }

    public void setPlacevendu(Placevendu[] placevendu) {
        this.placevendu = placevendu;
    }
    

    public Double getDepense() {
        return depense;
    }

    public void setDepense(Double depense) {
        this.depense = depense;
    }

    public Elementdevis[] getElement() {
        return element;
    }

    public void setElement(Elementdevis[] element) {
        this.element = element;
    }
    
    
    


public void   getNbPlace() throws Exception{
    ServiceNombrecategorieplace snb=new ServiceNombrecategorieplace();
    Nombrecategorieplace p=new Nombrecategorieplace();
    p.setIdlieu(this.getLieu());
    setListecategorie(snb.filter(p));
}

public void calculerRecette(){
    Double retour=0d;
    for(Nombrecategorieplace i:listecategorie){
        Double temp=0d;
        for(PrixplaceDevis j:getPriceplace()){
            if(Objects.equals(j.getCategorieplace(), i.getIdcategorieplace())){
                temp=j.getPrix()*i.getNb();
                break;
            }
        }
        retour+=temp;
    }
    recette=retour;
}
public void calculerdepense(){
    Double retour=0d;
    for(Elementdevis i:element){
        retour+=i.getPrix()*i.getDuree()*i.getQuantite();
    }
    depense= retour;
}
    public Nombrecategorieplace[] getListecategorie() {
        return listecategorie;
    }

    public void setListecategorie(Nombrecategorieplace[] listecategorie) {
        this.listecategorie = listecategorie;
    }

    public Double getRecette() {
        return recette;
    }

    public void setRecette(Double recette) {
        this.recette = recette;
    }
    
    
}
