/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import modele.view.PrixplaceDevis;

/**
 *
 * @author andri
 */
public class NbPlaceHelper {
    Integer lieu;
    PrixplaceDevis[] priceplace;

    public Integer getLieu() {
        return lieu;
    }

    public void setLieu(Integer lieu) {
        this.lieu = lieu;
    }

    
    public PrixplaceDevis[] getPriceplace() {
        return priceplace;
    }

    public void setPriceplace(PrixplaceDevis[] priceplace) {
        this.priceplace = priceplace;
    }
    
}
