/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prestation;

import modele.view.Devis;
import modele.view.Taxe;
import service.ServiceDevis;
import service.ServiceTaxe;

/**
 *
 * @author andri
 */
public class Benefice {
    Devis devis;
    Taxe taxe;
    Double benefice=0d;
    Recette recette;
    Double taxevalue;
    Double benefixenet=0d;
    public void initialise(Devis d) throws Exception{
          ServiceDevis sd=new ServiceDevis();
        recette=new Recette();
        recette.setElement(sd.getElementdevis(d));
        recette.setPlacevendu(sd.getPlacevendu(d));
        recette.setPriceplace(sd.getPrix(d));
        recette.calculerdepense();
        recette.calculerRealRecette();
        recette.calculbenefice();
        benefice=recette.getBenefice();
    }
    public void getTax() throws Exception{
        ServiceTaxe t=new ServiceTaxe();
        taxe=t.useQuery().select(Taxe.class).orderBy("desc", "maxvalue").execute()[0];
    }
    public void beneficeNet(){
        benefixenet=benefice-taxevalue;
    }
    public void calculTax(){
        taxevalue=benefice*(taxe.getPourcentage())/100;
//        for(Taxe i:taxe){
//            if(benefice>i.getMaxvalue()){
//                taxevalue=benefice*(i.getPourcentage())/100;
//                break;
//            }
//        }
    }

    public Devis getDevis() {
        return devis;
    }

    public void setDevis(Devis devis) {
        this.devis = devis;
    }

    public Taxe getTaxe() {
        return taxe;
    }

    public void setTaxe(Taxe taxe) {
        this.taxe = taxe;
    }

    public Double getBenefice() {
        return benefice;
    }

    public void setBenefice(Double benefice) {
        this.benefice = benefice;
    }

    public Recette getRecette() {
        return recette;
    }

    public void setRecette(Recette recette) {
        this.recette = recette;
    }

    public Double getTaxevalue() {
        return taxevalue;
    }

    public void setTaxevalue(Double taxevalue) {
        this.taxevalue = taxevalue;
    }

    public Double getBenefixenet() {
        return benefixenet;
    }

    public void setBenefixenet(Double benefixenet) {
        this.benefixenet = benefixenet;
    }
    
}
