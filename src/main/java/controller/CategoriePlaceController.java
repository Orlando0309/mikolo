package controller;

import modele.search.CategoriePlaceSearch;
import modele.view.CategoriePlace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceCategoriePlace;

/**
 *
 * @author andri
 */
@RequestMapping("/categorieplace")
@Component
@RestController
@CrossOrigin(value = "*")
public class CategoriePlaceController extends ScaffoldController<CategoriePlace, ServiceCategoriePlace, CategoriePlaceSearch, ServiceAdmin> {
    
    @Autowired
    public CategoriePlaceController(ServiceCategoriePlace service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, CategoriePlace.class, new CategoriePlaceSearch(CategoriePlace.class), tokenable, resourceLoader);
    }
    
}
