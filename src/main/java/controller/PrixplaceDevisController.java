package controller;

import modele.search.PrixplaceDevisSearch;
import modele.view.PrixplaceDevis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServicePrixplaceDevis;

/**
 *
 * @author andri
 */
@RequestMapping("/prixplacedevis")
@Component
@RestController
@CrossOrigin(value = "*")
public class PrixplaceDevisController extends ScaffoldController<PrixplaceDevis, ServicePrixplaceDevis, PrixplaceDevisSearch, ServiceEmploye> {
    
    @Autowired
    public PrixplaceDevisController(ServicePrixplaceDevis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, PrixplaceDevis.class, new PrixplaceDevisSearch(PrixplaceDevis.class), tokenable, resourceLoader);
    }
    
}
