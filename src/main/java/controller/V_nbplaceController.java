package controller;

import modele.search.V_nbplaceSearch;
import modele.view.V_nbplace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceV_nbplace;

/**
 *
 * @author andri
 */
@RequestMapping("/v_nbplace")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_nbplaceController extends ScaffoldController<V_nbplace, ServiceV_nbplace, V_nbplaceSearch, ServiceAdmin> {
    
    @Autowired
    public V_nbplaceController(ServiceV_nbplace service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, V_nbplace.class, new V_nbplaceSearch(V_nbplace.class), tokenable, resourceLoader);
    }
    
}
