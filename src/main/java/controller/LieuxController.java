package controller;

import modele.search.LieuxSearch;
import modele.view.Lieux;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceLieux;

/**
 *
 * @author andri
 */
@RequestMapping("/lieux")
@Component
@RestController
@CrossOrigin(value = "*")
public class LieuxController extends ScaffoldController<Lieux, ServiceLieux, LieuxSearch, ServiceAdmin> {
    
    @Autowired
    public LieuxController(ServiceLieux service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Lieux.class, new LieuxSearch(Lieux.class), tokenable, resourceLoader);
    }
    
}
