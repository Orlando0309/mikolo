package controller;

import modele.search.TransportSearch;
import modele.view.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceTransport;

/**
 *
 * @author andri
 */
@RequestMapping("/transport")
@Component
@RestController
@CrossOrigin(value = "*")
public class TransportController extends ScaffoldController<Transport, ServiceTransport, TransportSearch, ServiceAdmin> {
    
    @Autowired
    public TransportController(ServiceTransport service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Transport.class, new TransportSearch(Transport.class), tokenable, resourceLoader);
    }
    
}
