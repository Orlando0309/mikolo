package controller;

import modele.search.ArtisteSearch;
import modele.view.Artiste;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceArtiste;

/**
 *
 * @author andri
 */
@RequestMapping("/artiste")
@Component
@RestController
@CrossOrigin(value = "*")
public class ArtisteController extends ScaffoldController<Artiste, ServiceArtiste, ArtisteSearch, ServiceAdmin> {
    
    @Autowired
    public ArtisteController(ServiceArtiste service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Artiste.class, new ArtisteSearch(Artiste.class), tokenable, resourceLoader);
    }
    
}
