package controller;

import com.google.gson.Gson;
import modele.search.DevisSearch;
import modele.view.Devis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import prestation.DevisHelper;
import scaffold.ScaffoldController;
import scaffold.response.StatuedResponse;
import service.ServiceEmploye;
import service.ServiceDevis;

/**
 *
 * @author andri
 */
@RequestMapping("/devis")
@Component
@RestController
@CrossOrigin(value = "*")
public class DevisController extends ScaffoldController<Devis, ServiceDevis, DevisSearch, ServiceEmploye> {

    @Autowired
    public DevisController(ServiceDevis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, Devis.class, new DevisSearch(), tokenable, resourceLoader);
    }

    @ResponseBody
    @PostMapping("/savedevis")
    public StatuedResponse saveDevis(@RequestHeader("Authorization") String token, @RequestBody(required = true) DevisHelper helper) {
        StatuedResponse retour = new StatuedResponse();
//        retour.setMessage((new Gson()).toJson(helper))
        try {
            helper.save();
            retour.setMessage("Success");
        } catch (Exception ex) {
            ex.printStackTrace();
            retour.setMessage(ex.getMessage());
        }
        return retour;
    }
}
