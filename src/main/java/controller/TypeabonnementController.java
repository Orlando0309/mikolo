package controller;

import modele.search.TypeabonnementSearch;
import modele.view.Typeabonnement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceTypeabonnement;

/**
 *
 * @author andri
 */
@RequestMapping("/typeabonnement")
@Component
@RestController
@CrossOrigin(value = "*")
public class TypeabonnementController extends ScaffoldController<Typeabonnement, ServiceTypeabonnement, TypeabonnementSearch, ServiceAdmin> {
    
    @Autowired
    public TypeabonnementController(ServiceTypeabonnement service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Typeabonnement.class, new TypeabonnementSearch(Typeabonnement.class), tokenable, resourceLoader);
    }
    
}
