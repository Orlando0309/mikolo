package controller;

import modele.search.ElementprestationSearch;
import modele.view.Elementprestation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceElementprestation;

/**
 *
 * @author andri
 */
@RequestMapping("/elementprestation")
@Component
@RestController
@CrossOrigin(value = "*")
public class ElementprestationController extends ScaffoldController<Elementprestation, ServiceElementprestation, ElementprestationSearch, ServiceAdmin> {
    
    @Autowired
    public ElementprestationController(ServiceElementprestation service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Elementprestation.class, new ElementprestationSearch(Elementprestation.class), tokenable, resourceLoader);
    }
    
}
