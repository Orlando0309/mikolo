package controller;

import modele.search.CommunicationSearch;
import modele.view.Communication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceCommunication;

/**
 *
 * @author andri
 */
@RequestMapping("/communication")
@Component
@RestController
@CrossOrigin(value = "*")
public class CommunicationController extends ScaffoldController<Communication, ServiceCommunication, CommunicationSearch, ServiceAdmin> {
    
    @Autowired
    public CommunicationController(ServiceCommunication service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Communication.class, new CommunicationSearch(Communication.class), tokenable, resourceLoader);
    }
    
}
