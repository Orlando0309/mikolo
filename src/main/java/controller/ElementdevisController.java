package controller;

import modele.search.ElementdevisSearch;
import modele.view.Elementdevis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceElementdevis;

/**
 *
 * @author andri
 */
@RequestMapping("/elementdevisdevis")
@Component
@RestController
@CrossOrigin(value = "*")
public class ElementdevisController extends ScaffoldController<Elementdevis, ServiceElementdevis, ElementdevisSearch, ServiceEmploye> {
    
    @Autowired
    public ElementdevisController(ServiceElementdevis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, Elementdevis.class, new ElementdevisSearch(Elementdevis.class), tokenable, resourceLoader);
    }
    
}
