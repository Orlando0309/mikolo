package controller;

import modele.search.V_elementdevis_prestation_devisSearch;
import modele.view.V_elementdevis_prestation_devis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceV_elementdevis_prestation_devis;

/**
 *
 * @author andri
 */
@RequestMapping("/v_elementdevis_prestation_devis")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_elementdevis_prestation_devisController extends ScaffoldController<V_elementdevis_prestation_devis, ServiceV_elementdevis_prestation_devis, V_elementdevis_prestation_devisSearch, ServiceEmploye> {
    
    @Autowired
    public V_elementdevis_prestation_devisController(ServiceV_elementdevis_prestation_devis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_elementdevis_prestation_devis.class, new V_elementdevis_prestation_devisSearch(V_elementdevis_prestation_devis.class), tokenable, resourceLoader);
    }
    
}
