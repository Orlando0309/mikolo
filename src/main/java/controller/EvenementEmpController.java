/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.strawberry.query.QueryDescriptor;
import modele.search.EvenementSearch;
import modele.view.Evenement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import static scaffold.LoginController.bearer;
import scaffold.ScaffoldController;
import scaffold.response.ScaffoldResponse;
import service.ServiceAdmin;
import service.ServiceEmploye;
import service.ServiceEvenement;

/**
 *
 * @author andri
 */
@RequestMapping("/evenementemp")
@Component
@RestController
@CrossOrigin(value = "*")
public class EvenementEmpController extends ScaffoldController<Evenement, ServiceEvenement, EvenementSearch, ServiceEmploye> {
    
       @Autowired
    public EvenementEmpController(ServiceEvenement service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, Evenement.class, new EvenementSearch(Evenement.class), tokenable, resourceLoader);
    }
    @ResponseBody
    @PostMapping("/all2")
    public ScaffoldResponse<Evenement[]> listeAll(@RequestHeader("Authorization") String token) throws Exception {

        boolean teste = getTokenable().isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        ScaffoldResponse<Evenement[]> result = new ScaffoldResponse();
        QueryDescriptor<Evenement> de = getService().useQuery().select(Evenement.class);

        result.setData((Evenement[]) de.orderBy("asc", "id").execute());
        return result;
    }
}
