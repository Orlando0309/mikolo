package controller;

import modele.search.TypelieuSearch;
import modele.view.Typelieu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceTypelieu;

/**
 *
 * @author andri
 */
@RequestMapping("/typelieu")
@Component
@RestController
@CrossOrigin(value = "*")
public class TypelieuController extends ScaffoldController<Typelieu, ServiceTypelieu, TypelieuSearch, ServiceAdmin> {
    
    @Autowired
    public TypelieuController(ServiceTypelieu service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Typelieu.class, new TypelieuSearch(Typelieu.class), tokenable, resourceLoader);
    }
    
}
