package controller;

import com.strawberry.criteria.StrawCriteria;
import com.strawberry.criteria.StrawRestrictions;
import modele.search.PrestationCompletSearch;
import modele.view.PrestationComplet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import scaffold.response.ScaffoldResponse;
import service.ServiceEmploye;
import service.ServicePrestationComplet;

/**
 *
 * @author andri
 */
@RequestMapping("/prestcomp")
@Component
@RestController
@CrossOrigin(value = "*")
public class PrestationCompletController extends ScaffoldController<PrestationComplet, ServicePrestationComplet, PrestationCompletSearch, ServiceEmploye> {
    
    @Autowired
    public PrestationCompletController(ServicePrestationComplet service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, PrestationComplet.class, new PrestationCompletSearch(PrestationComplet.class), tokenable, resourceLoader);
    }
    
    @ResponseBody
    @PostMapping("/all2/{id}")
    public ScaffoldResponse<PrestationComplet[]> get(@RequestHeader("Authorization") String token, @PathVariable(name = "id") String id) throws Exception{
        StrawCriteria criteria=new StrawCriteria();
        System.out.println("id="+id);
        PrestationComplet p=new PrestationComplet();
        p.setPrestation_id(Integer.parseInt(id));
        criteria.add(StrawRestrictions.eq("prestation_id", Integer.parseInt(id)));
        PrestationComplet[] liste=this.getService().filter(p);
        ScaffoldResponse<PrestationComplet[]> result = new ScaffoldResponse();
        result.setData(liste);
        return result;
    }
    
}
