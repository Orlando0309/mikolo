package controller;

import modele.search.TypetarifSearch;
import modele.view.Typetarif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceTypetarif;

/**
 *
 * @author andri
 */
@RequestMapping("/typetarif")
@Component
@RestController
@CrossOrigin(value = "*")
public class TypetarifController extends ScaffoldController<Typetarif, ServiceTypetarif, TypetarifSearch, ServiceAdmin> {
    
    @Autowired
    public TypetarifController(ServiceTypetarif service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Typetarif.class, new TypetarifSearch(Typetarif.class), tokenable, resourceLoader);
    }
    
}
