package controller;

import modele.search.ArtistsSearch;
import modele.view.Artists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceArtists;

/**
 *
 * @author andri
 */
@RequestMapping("/artists")
@Component
@RestController
@CrossOrigin(value = "*")
public class ArtistsController extends ScaffoldController<Artists, ServiceArtists, ArtistsSearch, ServiceAdmin> {
    
    @Autowired
    public ArtistsController(ServiceArtists service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Artists.class, new ArtistsSearch(Artists.class), tokenable, resourceLoader);
    }
    
}
