package controller;

import modele.search.EmployeSearch;
import modele.view.Employe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.LoginController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceEmploye;

/**
 *
 * @author andri
 */
@RequestMapping("/emp")
@Component
@RestController
@CrossOrigin(value = "*")
public class EmployeController extends LoginController<Employe,ServiceEmploye> {
    
    public EmployeController(ResourceLoader resourceLoader) throws Exception {
        super(resourceLoader, new ServiceEmploye());
    }

    
}
