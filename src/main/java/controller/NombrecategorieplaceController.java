package controller;

import modele.search.NombrecategorieplaceSearch;
import modele.view.Nombrecategorieplace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceNombrecategorieplace;

/**
 *
 * @author andri
 */
@RequestMapping("/nombreategorieplace")
@Component
@RestController
@CrossOrigin(value = "*")
public class NombrecategorieplaceController extends ScaffoldController<Nombrecategorieplace, ServiceNombrecategorieplace, NombrecategorieplaceSearch, ServiceAdmin> {
    
    @Autowired
    public NombrecategorieplaceController(ServiceNombrecategorieplace service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Nombrecategorieplace.class, new NombrecategorieplaceSearch(Nombrecategorieplace.class), tokenable, resourceLoader);
    }
    
}
