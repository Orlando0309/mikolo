package controller;

import modele.search.V_artiste_spectacleSearch;
import modele.view.V_artiste_spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceV_artiste_spectacle;

/**
 *
 * @author andri
 */
@RequestMapping("/v_artiste_spectacle")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_artiste_spectacleController extends ScaffoldController<V_artiste_spectacle, ServiceV_artiste_spectacle, V_artiste_spectacleSearch, ServiceEmploye> {
    
    @Autowired
    public V_artiste_spectacleController(ServiceV_artiste_spectacle service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_artiste_spectacle.class, new V_artiste_spectacleSearch(V_artiste_spectacle.class), tokenable, resourceLoader);
    }
    
}
