package controller;

import modele.search.V_elementdevis_prestationSearch;
import modele.view.V_elementdevis_prestation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceV_elementdevis_prestation;

/**
 *
 * @author andri
 */
@RequestMapping("/v_elementdevis_prestation")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_elementdevis_prestationController extends ScaffoldController<V_elementdevis_prestation, ServiceV_elementdevis_prestation, V_elementdevis_prestationSearch, ServiceEmploye> {
    
    @Autowired
    public V_elementdevis_prestationController(ServiceV_elementdevis_prestation service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_elementdevis_prestation.class, new V_elementdevis_prestationSearch(V_elementdevis_prestation.class), tokenable, resourceLoader);
    }
    
}
