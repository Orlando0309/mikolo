package controller;

import modele.search.LieuSearch;
import modele.view.Lieu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceLieu;

/**
 *
 * @author andri
 */
@RequestMapping("/lieu")
@Component
@RestController
@CrossOrigin(value = "*")
public class LieuController extends ScaffoldController<Lieu, ServiceLieu, LieuSearch, ServiceAdmin> {
    
    @Autowired
    public LieuController(ServiceLieu service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Lieu.class, new LieuSearch(Lieu.class), tokenable, resourceLoader);
    }
    
}
