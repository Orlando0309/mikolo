package controller;

import modele.search.V_lieux_spectacleSearch;
import modele.view.V_lieux_spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceV_lieux_spectacle;

/**
 *
 * @author andri
 */
@RequestMapping("/V_lieux_spectacle")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_lieux_spectacleController extends ScaffoldController<V_lieux_spectacle, ServiceV_lieux_spectacle, V_lieux_spectacleSearch, ServiceEmploye> {
    
    @Autowired
    public V_lieux_spectacleController(ServiceV_lieux_spectacle service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_lieux_spectacle.class, new V_lieux_spectacleSearch(V_lieux_spectacle.class), tokenable, resourceLoader);
    }
    
}
