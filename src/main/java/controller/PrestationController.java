package controller;

import modele.search.PrestationSearch;
import modele.view.Prestation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServicePrestation;

/**
 *
 * @author andri
 */
@RequestMapping("/prestation")
@Component
@RestController
@CrossOrigin(value = "*")
public class PrestationController extends ScaffoldController<Prestation, ServicePrestation, PrestationSearch, ServiceAdmin> {
    
    @Autowired
    public PrestationController(ServicePrestation service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Prestation.class, new PrestationSearch(Prestation.class), tokenable, resourceLoader);
    }
    
}
