/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import modele.search.CategoriePlaceSearch;
import modele.view.CategoriePlace;
import modele.view.Devis;
import modele.view.Placevendu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import prestation.Benefice;
import prestation.NbPlaceHelper;
import prestation.Recette;
import prestation.StatisitiqueSpectacle;
import scaffold.ScaffoldController;
import scaffold.response.ScaffoldResponse;
import scaffold.response.StatuedResponse;
import service.ServiceAdmin;
import service.ServiceCategoriePlace;
import service.ServiceDevis;
import service.ServiceEmploye;
import service.ServicePlacevendu;

/**
 *
 * @author andri
 */
@RequestMapping("/empcategorieplace")
@Component
@RestController
@CrossOrigin(value = "*")
public class CategoriePlaceEmpController extends ScaffoldController<CategoriePlace, ServiceCategoriePlace, CategoriePlaceSearch, ServiceEmploye> {
        @Autowired
    public CategoriePlaceEmpController(ServiceCategoriePlace service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, CategoriePlace.class, new CategoriePlaceSearch(CategoriePlace.class), tokenable, resourceLoader);
    }
    

      @ResponseBody
    @PostMapping("/vendu")    
    public StatuedResponse insertPlace(@RequestHeader("Authorization") String token, @RequestBody Recette helper) throws Exception{
        Placevendu[] vendu=helper.getPlacevendu();
        ServicePlacevendu serp=new ServicePlacevendu();
        StatuedResponse retour=new StatuedResponse();
        try{
        serp.saveliste(vendu);
        }catch(Exception ex){
            retour.setMessage(ex.getMessage());
            ex.printStackTrace();
        }
        return retour;
    }
      @ResponseBody
    @PostMapping("/recette")
    public ScaffoldResponse<Recette> liste(@RequestHeader("Authorization") String token, @RequestBody Recette helper) throws Exception{
//        Recette recette=new Recette();
//        recette.setLieu(helper.getLieu());
//        recette.
        ScaffoldResponse<Recette> retour=new ScaffoldResponse<>();
        System.out.println("Toerana=="+helper.getLieu());
        helper.getNbPlace();
        helper.calculerRecette();
        retour.setData(helper);
        return retour;
    }
      @ResponseBody
    @PostMapping("/depense")
    public ScaffoldResponse<Recette> dep(@RequestHeader("Authorization") String token, @RequestBody Recette helper) throws Exception{
        ScaffoldResponse<Recette> retour=new ScaffoldResponse<>();
        System.out.println("Toerana=="+helper.getLieu());
        helper.getNbPlace();
        helper.calculerdepense();
        retour.setData(helper);
        return retour;
    }
     
      @ResponseBody
    @PostMapping("/spec")
    public ScaffoldResponse<StatisitiqueSpectacle> ben(@RequestHeader("Authorization") String token) throws Exception{
        ScaffoldResponse<StatisitiqueSpectacle> retour=new ScaffoldResponse<>();
        StatisitiqueSpectacle t=new StatisitiqueSpectacle();
        t.init();
//          ServiceDevis sd=new ServiceDevis();
//        Recette recette=new Recette();
//        recette.setElement(sd.getElementdevis(d));
//        recette.setPlacevendu(sd.getPlacevendu(d));
//        retour.setData(recette);
//        recette.setPriceplace(sd.getPrix(d));
//        recette.calculerdepense();
//        recette.calculerRealRecette();
//        recette.calculbenefice();
//        System.out.println("Toerana=="+helper.getLieu());
//        helper.getNbPlace();
//        helper.calculerdepense();
           
    
        retour.setData(t);
        return retour;
    }
}
