package controller;

import modele.search.EvenementSearch;
import modele.view.Evenement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceEvenement;

/**
 *
 * @author andri
 */
@RequestMapping("/evenement")
@Component
@RestController
@CrossOrigin(value = "*")
public class EvenementController extends ScaffoldController<Evenement, ServiceEvenement, EvenementSearch, ServiceAdmin> {
    
    @Autowired
    public EvenementController(ServiceEvenement service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Evenement.class, new EvenementSearch(Evenement.class), tokenable, resourceLoader);
    }
    
}
