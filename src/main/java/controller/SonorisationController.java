package controller;

import modele.search.SonorisationSearch;
import modele.view.Sonorisation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceSonorisation;

/**
 *
 * @author andri
 */
@RequestMapping("/sonorisation")
@Component
@RestController
@CrossOrigin(value = "*")
public class SonorisationController extends ScaffoldController<Sonorisation, ServiceSonorisation, SonorisationSearch, ServiceAdmin> {
    
    @Autowired
    public SonorisationController(ServiceSonorisation service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Sonorisation.class, new SonorisationSearch(Sonorisation.class), tokenable, resourceLoader);
    }
    
}
