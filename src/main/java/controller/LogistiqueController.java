package controller;

import modele.search.LogistiqueSearch;
import modele.view.Logistique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceLogistique;

/**
 *
 * @author andri
 */
@RequestMapping("/logistique")
@Component
@RestController
@CrossOrigin(value = "*")
public class LogistiqueController extends ScaffoldController<Logistique, ServiceLogistique, LogistiqueSearch, ServiceAdmin> {
    
    @Autowired
    public LogistiqueController(ServiceLogistique service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Logistique.class, new LogistiqueSearch(Logistique.class), tokenable, resourceLoader);
    }
    
}
