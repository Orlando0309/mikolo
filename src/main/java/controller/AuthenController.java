/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import modele.view.Admin;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.LoginController;
import service.ServiceAdmin;

/**
 *
 * @author andri
 */
@RequestMapping("/user")
@RestController
@CrossOrigin(value = "*")
public class AuthenController extends LoginController<Admin,ServiceAdmin> {
    
    
    public AuthenController(ResourceLoader resourceLoader) throws Exception {
        super(resourceLoader, new ServiceAdmin());
    }
    
}
