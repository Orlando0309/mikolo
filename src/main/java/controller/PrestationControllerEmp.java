/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import modele.search.PrestationSearch;
import modele.view.Prestation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import prestation.AllPrestation;
import static scaffold.LoginController.bearer;
import scaffold.ScaffoldController;
import scaffold.response.ScaffoldResponse;
import service.ServiceAdmin;
import service.ServiceEmploye;
import service.ServicePrestation;

/**
 *
 * @author andri
 */
@RequestMapping("/prestemp")
@Component
@RestController
@CrossOrigin(value = "*")
public class PrestationControllerEmp extends ScaffoldController<Prestation, ServicePrestation, PrestationSearch, ServiceEmploye> {
    
    @Autowired
    public PrestationControllerEmp(ServicePrestation service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
     super(service, Prestation.class, new PrestationSearch(Prestation.class), tokenable, resourceLoader);
    }
    
     @ResponseBody
    @PostMapping("/all2")
    public ScaffoldResponse<AllPrestation> get(@RequestHeader("Authorization") String token) throws Exception{
        boolean teste = getTokenable().isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        ScaffoldResponse<AllPrestation> result = new ScaffoldResponse();
        AllPrestation prst=new AllPrestation();
        prst.init();
        result.setData(prst);
        return result;
    }
    
}
