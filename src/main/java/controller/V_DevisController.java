package controller;

import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.http.HttpServletResponse;
import modele.search.V_DevisSearch;
import modele.view.Devis;
import modele.view.V_Devis;
import modele.view.V_artiste_spectacle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import prestation.AfficherPdf;
import scaffold.ScMarker;
import scaffold.ScaffoldController;
import scaffold.handler.ScHandler;
import service.ServiceDevis;
import service.ServiceEmploye;
import service.ServiceV_Devis;

/**
 *
 * @author andri
 */
@RequestMapping("/v_devis")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_DevisController extends ScaffoldController<V_Devis, ServiceV_Devis, V_DevisSearch, ServiceEmploye> {
    
    @Autowired
    public V_DevisController(ServiceV_Devis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_Devis.class, new V_DevisSearch(V_Devis.class), tokenable, resourceLoader);
    }
    
    @ResponseBody
    @PostMapping(value = "/affiche/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> generatePDF(@PathVariable(name = "id") String id,HttpServletResponse http) throws Exception{
        Devis devis=new Devis();
        devis.setId(Integer.parseInt(id));
        ServiceDevis ser=new ServiceDevis();
        devis=ser.getRow(devis);
        AfficherPdf h=new AfficherPdf();
        h.setDevis(devis);
        System.out.println(generate(h));
          try {
            http.setHeader("Content-Disposition", "attachement; filename=Devis.pdf");
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
                    .body(ScHandler.toPdf(generate(h), "Devis.pdf"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String generate(AfficherPdf af) throws Exception{
        ScMarker marker=new ScMarker("affiche.ftl");
        marker.put("affiche", af);
        marker.put("titre", af.getDevis().getNomevenement());
        marker.put("daty", af.getDevis().getDaty());
        marker.put("artists", Arrays.asList(af.liste()));
        String contents=marker.getContent();
        return contents;
    }
    
    
}
