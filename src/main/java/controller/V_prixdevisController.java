package controller;

import modele.search.V_prixdevisSearch;
import modele.view.V_prixdevis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServiceV_prixdevis;

/**
 *
 * @author andri
 */
@RequestMapping("/v_prixdevis")
@Component
@RestController
@CrossOrigin(value = "*")
public class V_prixdevisController extends ScaffoldController<V_prixdevis, ServiceV_prixdevis, V_prixdevisSearch, ServiceEmploye> {
    
    @Autowired
    public V_prixdevisController(ServiceV_prixdevis service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, V_prixdevis.class, new V_prixdevisSearch(V_prixdevis.class), tokenable, resourceLoader);
    }
    
}
