package controller;

import modele.search.TaxeSearch;
import modele.view.Taxe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceAdmin;
import service.ServiceTaxe;

/**
 *
 * @author andri
 */
@RequestMapping("/taxe")
@Component
@RestController
@CrossOrigin(value = "*")
public class TaxeController extends ScaffoldController<Taxe, ServiceTaxe, TaxeSearch, ServiceAdmin> {
    
    @Autowired
    public TaxeController(ServiceTaxe service, ServiceAdmin tokenable, ResourceLoader resourceLoader) {
        super(service, Taxe.class, new TaxeSearch(Taxe.class), tokenable, resourceLoader);
    }
    
}
