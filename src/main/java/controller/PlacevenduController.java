package controller;

import modele.search.PlacevenduSearch;
import modele.view.Placevendu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scaffold.ScaffoldController;
import service.ServiceEmploye;
import service.ServicePlacevendu;

/**
 *
 * @author andri
 */
@RequestMapping("/placevendu")
@Component
@RestController
@CrossOrigin(value = "*")
public class PlacevenduController extends ScaffoldController<Placevendu, ServicePlacevendu, PlacevenduSearch, ServiceEmploye> {
    
    @Autowired
    public PlacevenduController(ServicePlacevendu service, ServiceEmploye tokenable, ResourceLoader resourceLoader) {
        super(service, Placevendu.class, new PlacevenduSearch(Placevendu.class), tokenable, resourceLoader);
    }
    
    
    
}
