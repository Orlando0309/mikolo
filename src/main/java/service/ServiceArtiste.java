/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Artiste;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceArtiste extends BerryTree<Artiste> {
    
    public ServiceArtiste() throws Exception {
        super(Artiste.class);
    }
    
}
