/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_prixdevis;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_prixdevis extends BerryTree<V_prixdevis> {
    
    public ServiceV_prixdevis() throws Exception {
        super(V_prixdevis.class);
    }
    
}
