/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Devis;
import modele.view.Elementdevis;
import modele.view.Placevendu;
import modele.view.PrixplaceDevis;
import modele.view.V_lieux_spectacle;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceDevis extends BerryTree<Devis> {

    public ServiceDevis() throws Exception {
        super(Devis.class);
    }

    public Integer getlieu(Devis devis) throws Exception {
        V_lieux_spectacle s = new V_lieux_spectacle();
        ServiceV_lieux_spectacle spec = new ServiceV_lieux_spectacle();
        s.setDevis(devis.getId());
        s = spec.getRow(s);
        return s.getValeurprestation();
    }
    public Elementdevis[] getElementdevis(Devis devis) throws Exception{
        ServiceElementdevis se=new ServiceElementdevis();
        Elementdevis temp=new Elementdevis();
        temp.setDevis(devis.getId());
        return se.filter(temp);
    }
    public Placevendu[] getPlacevendu(Devis devis) throws Exception{
        ServicePlacevendu p=new ServicePlacevendu();
        Placevendu temp=new Placevendu();
        temp.setDevis(devis.getId());
        return p.filter(temp);
    }
    
    public PrixplaceDevis[] getPrix(Devis devis) throws Exception{
        ServicePrixplaceDevis p=new ServicePrixplaceDevis();
        PrixplaceDevis temp=new PrixplaceDevis();
        temp.setDevis(devis.getId());
        return p.filter(temp);
    }

}
