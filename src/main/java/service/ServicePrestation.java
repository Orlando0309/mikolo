/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import com.strawberry.criteria.StrawCriteria;
import com.strawberry.criteria.StrawRestrictions;
import com.strawberry.session.StrawSession;
import modele.view.Prestation;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServicePrestation extends BerryTree<Prestation> {
    
    public ServicePrestation() throws Exception {
        super(Prestation.class);
    }
    
    public Prestation[] get(int fixe) throws Exception{
        StrawCriteria cr=StrawSession.createStrawCriteria(Prestation.class);
        cr.add(StrawRestrictions.eq("fixe", fixe));
        return this.filter(cr);
    }
    
}
