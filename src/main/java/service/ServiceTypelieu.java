/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Typelieu;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceTypelieu extends BerryTree<Typelieu> {
    
    public ServiceTypelieu() throws Exception {
        super(Typelieu.class);
    }
    
}
