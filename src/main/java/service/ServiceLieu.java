/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import constante.ConstanteEvent;
import modele.view.Elementprestation;
import modele.view.Lieu;
import modele.view.Typelieu;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceLieu extends BerryTree<Lieu> {
    
    public ServiceLieu() throws Exception {
        super(Lieu.class);
    }

    @Override
    public void save(Lieu o) throws Exception {
        super.save(o); //To change body of generated methods, choose Tools | Templates.
        
        ServiceElementprestation prestation=new ServiceElementprestation();
//        ElementPrestato
        ServiceTypelieu typelieu=new ServiceTypelieu();
        Typelieu tp=new Typelieu();
        tp.setId(o.getTypelieu());
        tp=typelieu.filter(tp)[0];
        
        Elementprestation element=new Elementprestation();
        element.setPrestation(ConstanteEvent.LIEUX);
        element.setPlace(o.getMaxplace());
        element.setNomelement(o.getNom()+"-"+tp.getNomtypelieu());
        element.setTypeabn(ConstanteEvent.STANDARD);
        
        prestation.save(element);
       
    }
    
    
}
