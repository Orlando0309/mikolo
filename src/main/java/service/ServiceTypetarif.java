/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Typetarif;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceTypetarif extends BerryTree<Typetarif> {
    
    public ServiceTypetarif() throws Exception {
        super(Typetarif.class);
    }
    
}
