/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import com.strawberry.constante.Constante;
import com.strawberry.criteria.StrawCriteria;
import com.strawberry.criteria.StrawRestrictions;
import com.strawberry.session.StrawSession;
import modele.view.Employe;
import org.springframework.stereotype.Service;
import scaffold.Tokenable;

/**
 *
 * @author andri
 */
@Service
public class ServiceEmploye extends BerryTree<Employe> implements Tokenable {

    public ServiceEmploye() throws Exception {
        super(Employe.class);
    }

    @Override
    public boolean isExpired(String token) throws Exception {
        if (token != null) {
            StrawCriteria criteria = StrawSession.createStrawCriteria(this.getClasse());
            criteria.add(StrawRestrictions.eq("token", token));
//            System.out.println(criteria.bloom());
            if (this.filter(criteria).length != 0) {
                criteria.add(StrawRestrictions.lt("expired_at", Constante.CURRENT_TIMESTAMP));
                return this.filter(criteria).length != 0;
            }
//        return true;
        }
        return true;
    }

}
