/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import com.strawberry.constante.Constante;
import com.strawberry.criteria.StrawCriteria;
import com.strawberry.criteria.StrawRestrictions;
import com.strawberry.session.StrawSession;
import modele.view.Admin;
import org.springframework.stereotype.Service;
import scaffold.Tokenable;

/**
 *
 * @author andri
 */
@Service
public class ServiceAdmin extends BerryTree<Admin> implements Tokenable {
    
    public ServiceAdmin() throws Exception {
        super(Admin.class);
    }

    @Override
    public boolean isExpired(String token) throws Exception {
        StrawCriteria criteria = StrawSession.createStrawCriteria(this.getClasse());
//        System.out.println("token="+token+" "+this.getClasse());
        criteria.add(StrawRestrictions.eq("token", token));
        ServiceAdmin ad=new ServiceAdmin();
      
        if(ad.filter(criteria).length != 0){
        criteria.add(StrawRestrictions.lt("expired_at", Constante.CURRENT_TIMESTAMP));
            return ad.filter(criteria).length != 0;
        }
        return true;
//        return this.filter(criteria).length != 0;
    }
    
}
