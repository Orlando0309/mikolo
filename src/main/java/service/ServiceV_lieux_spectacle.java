/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_lieux_spectacle;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_lieux_spectacle extends BerryTree<V_lieux_spectacle> {
    
    public ServiceV_lieux_spectacle() throws Exception {
        super(V_lieux_spectacle.class);
    }
    
}
