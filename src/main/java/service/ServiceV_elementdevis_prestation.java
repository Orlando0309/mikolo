/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_elementdevis_prestation;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_elementdevis_prestation extends BerryTree<V_elementdevis_prestation> {
    
    public ServiceV_elementdevis_prestation() throws Exception {
        super(V_elementdevis_prestation.class);
    }
    
}
