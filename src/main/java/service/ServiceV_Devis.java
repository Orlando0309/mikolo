/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_Devis;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_Devis extends BerryTree<V_Devis> {
    
    public ServiceV_Devis() throws Exception {
        super(V_Devis.class);
    }
    
}
