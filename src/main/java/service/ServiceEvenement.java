/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Evenement;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceEvenement extends BerryTree<Evenement> {
    
    public ServiceEvenement() throws Exception {
        super(Evenement.class);
    }
    
}
