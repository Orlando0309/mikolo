/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_nbplace;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_nbplace extends BerryTree<V_nbplace> {
    
    public ServiceV_nbplace() throws Exception {
        super(V_nbplace.class);
    }
    
}
