/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Artists;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceArtists extends BerryTree<Artists> {
    
    public ServiceArtists() throws Exception {
        super(Artists.class);
    }
    
}
