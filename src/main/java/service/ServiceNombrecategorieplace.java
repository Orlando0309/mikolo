/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Lieux;
import modele.view.Nombrecategorieplace;
import modele.view.V_nbplace;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceNombrecategorieplace extends BerryTree<Nombrecategorieplace> {
    
    public ServiceNombrecategorieplace() throws Exception {
        super(Nombrecategorieplace.class);
    }

    @Override
    public void save(Nombrecategorieplace o) throws Exception {
        if(isReal(o))
        super.save(o); //To change body of generated methods, choose Tools | Templates.
        else{
            throw new Exception("Not enough place");
        }
    }
    
    
    
    public boolean isReal(Nombrecategorieplace o) throws Exception{
        ServiceV_nbplace nb=new ServiceV_nbplace();
        V_nbplace b=new V_nbplace();
        b.setId(o.getIdlieu());
        b=nb.getRow(b);
        Integer totalplace=b.getNombretotal();
        
        ServiceLieux l=new ServiceLieux();
        Lieux lieux=new Lieux();
        lieux.setId(o.getIdlieu());
        System.out.println("lieux:"+o.getIdlieu());
        lieux=l.getRow(lieux);
        Integer total=l.getRow(lieux).getPlace();
        return o.getNb()<=total-totalplace;
    }
    public boolean isReal2(Nombrecategorieplace o) throws Exception{
        ServiceV_nbplace nb=new ServiceV_nbplace();
        V_nbplace b=new V_nbplace();
        b.setId(o.getIdlieu());
        b=nb.getRow(b);
        Integer totalplace=b.getNombretotal();
        
        ServiceLieux l=new ServiceLieux();
        Lieux lieux=new Lieux();
        lieux.setId(o.getIdlieu());
        System.out.println("lieux:"+o.getIdlieu());
        lieux=l.getRow(lieux);
        Integer total=l.getRow(lieux).getPlace();
        return o.getNb()<=totalplace;
    }
}
