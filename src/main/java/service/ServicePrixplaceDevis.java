/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.PrixplaceDevis;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServicePrixplaceDevis extends BerryTree<PrixplaceDevis> {
    
    public ServicePrixplaceDevis() throws Exception {
        super(PrixplaceDevis.class);
    }
    
}
