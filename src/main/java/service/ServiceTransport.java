/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.Transport;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceTransport extends BerryTree<Transport> {
    
    public ServiceTransport() throws Exception {
        super(Transport.class);
    }
    
}
