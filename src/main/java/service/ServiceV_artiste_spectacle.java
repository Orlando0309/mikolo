/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.strawberry.bloom.BerryTree;
import modele.view.V_artiste_spectacle;
import org.springframework.stereotype.Service;

/**
 *
 * @author andri
 */
@Service
public class ServiceV_artiste_spectacle extends BerryTree<V_artiste_spectacle> {
    
    public ServiceV_artiste_spectacle() throws Exception {
        super(V_artiste_spectacle.class);
    }
    
}
