/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mikolo;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author andri
 */
public class Venteinitializer {

    public HashMap<String, Double> state = new HashMap<>();

    public static HashMap<String, Double> start() {

        HashMap<String, Double> retour = new HashMap<>();
        List<Month> months = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            months.add(Month.of(i));
        }
        // Print the names of all months
        for (Month month : months) {
            String monthName = month.getDisplayName(TextStyle.FULL, Locale.getDefault());
            System.out.println(monthName);
            retour.put(monthName, 0d);
        }
        return retour;
    }
    public static String getMonth(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int monthValue = localDate.getMonthValue();
        Month month = Month.of(monthValue);
        String monthName = month.getDisplayName(TextStyle.FULL, Locale.getDefault());
        return monthName;
    }
    public static Statistique[] toStatistique(HashMap<String,Double> val){
        Set<String> k=val.keySet();
        ArrayList<Statistique> retour=new ArrayList<>();
        for(String i:k){
            Statistique s=new Statistique();
            s.setMois(i);
            s.setVente(val.get(i));
            retour.add(s);
            System.out.println("Mois:"+i+" :"+val.get(i));
        }
        return retour.toArray(new Statistique[retour.size()]);
    }
    
    
}
