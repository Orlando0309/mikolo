/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mikolo;

/**
 *
 * @author andri
 */
public class StatsistiqueBenefice extends StatisqueImpl {
    Double perte;
    Double achat;

    public Double getPerte() {
        return perte;
    }

    public void setPerte(Double perte) {
        this.perte = perte;
    }

    public Double getAchat() {
        return achat;
    }

    public void setAchat(Double achat) {
        this.achat = achat;
    }
    
}
