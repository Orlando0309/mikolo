/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mikolo;

/**
 *
 * @author andri
 */
public class Statistique {
    String mois;
    Double vente=0d;

    public Statistique(String mois,Double vente) {
        this.mois = mois;
        this.vente=vente;
    }

    
    public Statistique() {
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public Double getVente() {
        return vente;
    }

    public void setVente(Double vente) {
        this.vente = vente;
    }
    
    
}
