/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converter.inter;

/**
 *
 * @author andri
 */
interface Converter {
    
    double convert(double value, String fromUnit, String toUnit);
}