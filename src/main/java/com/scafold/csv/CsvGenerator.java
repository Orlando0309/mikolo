/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scafold.csv;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

/**
 *
 * @author andri
 * @param <T>
 */
public abstract class CsvGenerator<T extends Object> {
    public abstract String[]  getHeader() throws Exception;
    public String generateCsv( List<String[]> rows) throws  Exception {
        StringWriter writer = new StringWriter();
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(this.getHeader()));
        for (String[] row : rows) {
            csvPrinter.printRecord((Object[]) row);
        }
        csvPrinter.flush();
        return writer.toString();
    }
    public List<String[]> toCsvValues(T[] objects) throws Exception{
        List<String[]> retour=new ArrayList<>();
        for(int i=0;i<objects.length;i++) retour.add(mapMyObjectToCsvValues(objects[i]));
        return retour;
    }
    public abstract String[] mapMyObjectToCsvValues(T obj) throws Exception;
}
