/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

/**
 *
 * @author andri
 */
public class GeneratorMapping {
    String table;
    String modele;
    String packages;
    String servicepackage;
    String packagesearch;
    String packagecontroller;
    String routeController;
    String tokenable;

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }
    

    public String getPackagecontroller() {
        return packagecontroller;
    }

    public void setPackagecontroller(String packagecontroller) {
        this.packagecontroller = packagecontroller;
    }

    public String getRouteController() {
        return routeController;
    }

    public void setRouteController(String routeController) {
        this.routeController = routeController;
    }

    public String getTokenable() {
        return tokenable;
    }

    public void setTokenable(String tokenable) {
        this.tokenable = tokenable;
    }
    
    
    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getServicepackage() {
        return servicepackage;
    }

    public void setServicepackage(String servicepackage) {
        this.servicepackage = servicepackage;
    }

    public String getPackagesearch() {
        return packagesearch;
    }

    public void setPackagesearch(String packagesearch) {
        this.packagesearch = packagesearch;
    }
    
    
}
