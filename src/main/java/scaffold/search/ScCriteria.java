/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.search;

/**
 *
 * @author andri
 */
public  class ScCriteria {
    String field;
    String type;//minmax,like,lt,mt

    public ScCriteria() {
    }

    public ScCriteria(String field, String type) {
        this.field = field;
        this.type = type;
    }

    
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
