/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.search;

import com.reflect.Reflection;
import com.strawberry.criteria.StrawCriteria;
import com.strawberry.criteria.StrawCriterion;
import com.strawberry.criteria.StrawRestrictions;
import com.strawberry.session.StrawSession;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import scaffold.annotation.Search;

/**
 *
 * @author andri
 * @param <T>
 */
/*
rehefa type= arrayOption: <select>

 */
public class SearchTools<T extends Object> {

    Class<T> clT;

    public SearchTools(Class<T> clT) {
        this.clT = clT;
    }

    public Class<T> getClT() {
        return clT;
    }

    public void setClT(Class<T> clT) {
        this.clT = clT;
    }

    //get the Field
    // create an instance of criteria
    //  1 loop fields
    // if minmax type
// 
    public StrawCriterion minmax(Field field, Object min, Object max) throws Exception {
        System.out.println("Min"+min);
        System.out.println(max);
        ArrayList<StrawCriterion> r=new ArrayList<>();
        if(min!=null){
            StrawCriterion me = StrawRestrictions.me(field.getName(), min);
            r.add(me);
        }
        if(max!=null){
        StrawCriterion lt = StrawRestrictions.le(field.getName(), max);
        r.add(lt);
        }
        return StrawRestrictions.and(r.toArray(new StrawCriterion[r.size()]));
    }

    public StrawCriterion like(Field field, Object... values) throws Exception {
        ArrayList<StrawCriterion> ilaina = new ArrayList<>();
        for (Object i : values) {
            StrawCriterion temp = StrawRestrictions.like(field.getName(), "%" + i + "%");
            ilaina.add(temp);
        }
        return StrawRestrictions.or(ilaina.toArray(new StrawCriterion[ilaina.size()]));
    }

    public StrawCriterion eq(Field field, Object... values) throws Exception {
        ArrayList<StrawCriterion> ilaina = new ArrayList<>();
        for (Object i : values) {
            StrawCriterion temp = StrawRestrictions.eq(field.getName(), i);
            ilaina.add(temp);
        }
        return StrawRestrictions.or(ilaina.toArray(new StrawCriterion[ilaina.size()]));
    }

    public void checker() throws Exception {
        Reflection reflection = new Reflection(this.getClass());
        Collection<Field> fields = reflection.getFields().values();
        for (Field f : fields) {
            if (f.isAnnotationPresent(Search.class)) {
                System.out.println("Field::" + f.getName());
                Method met = reflection.getter(f.getName());
                System.out.println("Get::" + met.getName());
                if (met != null) {
                    Object obj = met.invoke(this);
                    if (obj != null) {
                        System.out.println("Object:" + obj + " " + f.getName());
                        Search s = f.getAnnotation(Search.class);
                        System.out.println("Field:" + f.getName() + " " + s.value());
                    }

                }
            }
        }
    }

    public StrawCriteria search() throws Exception {
        Reflection reflection = new Reflection(this.getClass());
        Collection<Field> fields = reflection.getFields().values();
        StrawCriteria str = StrawSession.createStrawCriteria(clT);
        for (Field f : fields) {
            if (f.isAnnotationPresent(Search.class)) {
                System.out.println("Field::" + f.getName());
                Method met = reflection.getter(f.getName());
                System.out.println("Get::" + met.getName());
                if (met != null) {
                    Object obj = met.invoke(this);
                    if (obj != null && !obj.toString().isEmpty()) {
                        System.out.println("Object:" + obj + " " + f.getName());
                        Search s = f.getAnnotation(Search.class);
                        System.out.println("Field:" + f.getName() + " " + s.value());
                        switch (s.value()) {
                            case "equal":
                                if (obj.getClass().isArray()) {
                                    Object[] za = (Object[]) obj;
                                    str.add(StrawRestrictions.and(eq(f, za)));
                                } else {
                                    str.add(StrawRestrictions.and(eq(f, obj)));
                                }
                                break;
                            case "like":
                                if (obj.getClass().isArray()) {
                                    Object[] za = (Object[]) obj;
                                    str.add(StrawRestrictions.and(like(f, za)));
                                } else {
                                    str.add(StrawRestrictions.and(like(f, obj)));
                                }
                                break;
                            case "minmax":
                                if (obj.getClass().isArray()) {
                                    Object[] za = (Object[]) obj;
                                      str.add(StrawRestrictions.and(minmax(f, za[0], za[1])));
                                    
                                }
                                break;
                            default:
                                break;
                        }
                    }

                }
            }
        }
        return str;
    }

    public static boolean isArray(Field f) {
        return f.getType().isArray();
    }

}
