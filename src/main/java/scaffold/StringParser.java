/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

import java.util.Map;

/**
 *
 * @author andri
 */
public interface StringParser {
    public void parse(Map<String,String> data) throws Exception;
}
