/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

import com.strawberry.auth.impl.StrawAuthen;
import com.strawberry.auth.impl.TokenRoot;
import com.strawberry.bloom.BerryTree;
import com.strawberry.session.StrawData;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import scaffold.response.StatuedResponse;

/**
 *
 * @author andri
 * @param <M>
 * @param <MS>
 * @param <SA>
 * @param <T>
 * @param <TS>
 */
public class LoginController<M extends TokenRoot,MS extends BerryTree<M>>  {
    
    
     private final ResourceLoader resourceLoader;
    MS serviceauth;

    public MS getServiceauth() {
        return serviceauth;
    }

    public void setServiceauth(MS serviceauth) {
        this.serviceauth = serviceauth;
    }

    public LoginController(ResourceLoader resourceLoader, MS serviceauth) {
        this.resourceLoader = resourceLoader;
        this.serviceauth = serviceauth;
    }
    
    @PostConstruct
    public void init() {
        Resource resource = resourceLoader.getResource("classpath:straw.xml");
        try {
            String path = resource.getFile().getPath();
            StrawData.init(path);
        } catch (IOException ex) {
            Logger.getLogger(ScaffoldController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void sendUser(StatuedResponse retour,M admin,StrawAuthen<M> authen) throws Exception{
        authen.assign(serviceauth);
        M res=authen.getResult();
        System.out.println("za"+res);
            res.action("auth");
            admin.action("");
            
            serviceauth.update(admin, res);
            retour.setData(res);
    }
    @PostMapping("/auth")
    public StatuedResponse login(@RequestBody M admin) throws Exception{
        System.out.println("admin: "+admin);
        StatuedResponse retour=new StatuedResponse();
        StrawAuthen<M> authen =new StrawAuthen<>(admin,serviceauth);
        boolean check=authen.identity().password().check(serviceauth);
        System.out.println("check:"+check);
        if(!check){
            int status=authen.getStatus();
            retour.setMessage((status==766)?"Mot de Passe Incorrect":((status==765)?"Identifiant Incorrect":"Vous n'etes pas reconnu en tant qu'administrateur!!!"));
            retour.setStatus((status==766)?-1:((status==765)?0:1));
//            retour.setData(status);
        }else{
            sendUser(retour, admin,authen);
            retour.setStatus(1);
        }
        return retour;
    }
    
    @PostMapping("/exp")
    public StatuedResponse testeExpired(@RequestHeader("Authorization")  String token) throws Exception{
        StatuedResponse retour=new StatuedResponse();
        if(serviceauth instanceof Tokenable){
            Tokenable a=(Tokenable) serviceauth;
            String totest=(token.startsWith("Bearer "))?token.substring(7):token;
            retour.setData(expired(totest, a));
            retour.setMessage("isexpired");
        }
        return retour;
    }
    public static String  bearer(String token){
        return (token.startsWith("Bearer "))?token.substring(7):token;
    }
    public static boolean expired(String token,Tokenable serviceauth) throws Exception{
        if(serviceauth instanceof Tokenable){
            Tokenable a=(Tokenable) serviceauth;
            String totest=bearer(token);
            return a.isExpired(totest);
        }
        return true;
    }
}
