/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.response;

import com.strawberry.bloom.BerryTree;

/**
 *
 * @author andri
 * @param <T>
 */
public class Loader<T extends Object> {
    Class object;
    BerryTree<T> service;

    
    public Loader(Class object, BerryTree<T> service) {
        this.object = object;
        this.service = service;
    }

    public Class getObject() {
        return object;
    }

    public void setObject(Class object) {
        this.object = object;
    }

    public BerryTree<?> getService() {
        return service;
    }

    public void setService(BerryTree<T> service) {
        this.service = service;
    }
    
    
}
