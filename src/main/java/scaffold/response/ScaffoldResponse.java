/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.response;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author andri
 * @param <T>
 * @param <S>
 */
public class ScaffoldResponse<T extends Object> implements Serializable {
    T data;
    
    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }
    


    
    
}
