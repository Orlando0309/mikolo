/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

import com.reflect.Reflection;
import com.strawberry.bloom.BerryTree;
import com.strawberry.criteria.StrawCriteria;
import com.strawberry.query.QueryDescriptor;
import com.strawberry.session.StrawData;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import jve.gen.JVEList;
import jve.html.tag.HTMLtag;
import jve.html.tag.specific.table.Table;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import scaffold.handler.ScData;
import scaffold.handler.ScHandler;
import scaffold.response.Loader;
import scaffold.response.ScaffoldResponse;
import scaffold.response.StatuedResponse;
import scaffold.response.Wrapper;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.multipart.MultipartFile;
import static scaffold.LoginController.bearer;
import scaffold.handler.ScDataSearch;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 * @param <T>
 * @param <Cs>
 * @param <Se>
 * @param <TK>
 * @param <S>
 */
//@RestController
//@Component
//@CrossOrigin(value = "*")
public class ScaffoldController<T extends Object, S extends BerryTree<T>, Se extends SearchTools, TK extends Tokenable> {

    HashMap<String, Loader> routes = new HashMap<>();
    S service;
    Class<T> c;
    Se search;
    TK tokenable;

    public S getService() {
        return service;
    }

    public void setService(S service) {
        this.service = service;
    }

    public TK getTokenable() {
        return tokenable;
    }

    public void setTokenable(TK tokenable) {
        this.tokenable = tokenable;
    }

    private final ResourceLoader resourceLoader;

    public ScaffoldController(S service, Class<T> c, Se search, TK tokenable, ResourceLoader resourceLoader) {
        this.service = service;
        this.c = c;
        this.search = search;
        this.resourceLoader = resourceLoader;
        this.tokenable = tokenable;
    }

    @PostConstruct
    public void init() {
        Resource resource = resourceLoader.getResource("classpath:straw.xml");
        try {
            String path = resource.getFile().getPath();
            StrawData.init(path);
        } catch (IOException ex) {
            Logger.getLogger(ScaffoldController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @GetMapping("route")
    public String routes(Model model) {
        model.addAttribute("route", routes);
        routes.forEach((i, j) -> {
            System.out.println(i + "->" + j.getService() + " " + j.getObject());
        });

        return "lala";
    }

    @GetMapping("/export-csv")
    public ScaffoldResponse<T[]> exportCsv(@RequestHeader("Authorization") String token) throws Exception {
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        ScaffoldResponse<T[]> result = new ScaffoldResponse();
//     
//        BerryTree<T> tree = loader.getService();
        result.setData(service.list());
        return result;

    }

    @ResponseBody
    @PostMapping("/")
    public ScaffoldResponse<T[]> liste(@RequestHeader("Authorization") String token, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "2") int maxpage, @RequestBody(required = false) Se se) throws Exception {
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        ScaffoldResponse<T[]> result = new ScaffoldResponse();
        String[] fields = ScHandler.getShows(c);
        QueryDescriptor<T> de = service.useQuery();
        if (fields == null) {
            de = de.select(c).limit(maxpage).offset((page - 1) * maxpage);
        } else {
            de = de.select(c, fields).limit(maxpage).offset((page - 1) * maxpage);

        }
        if (se != null) {
            StrawCriteria cr = se.search();
            se.checker();
            System.out.println(se.toString());
            System.out.println("Cr" + cr.toString());
            if (cr != null) {
                de = de.where(cr);
            }
        }
        try {
            T[] aa = de.orderBy("asc", "id").execute();
            System.out.println("AAA:" + aa.length);
            result.setData(aa);
        } catch (Exception ex) {
            ex.printStackTrace();
            result.setData(toT(2));
        }
        return result;
    }

    public T[] toT(int size) {
        return (T[]) Array.newInstance(this.getClass(), size);
    }

    @ResponseBody
    @PostMapping("/all")
    public ScaffoldResponse<T[]> listeAll(@RequestHeader("Authorization") String token, @RequestBody(required = false) Se se) throws Exception {

        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        ScaffoldResponse<T[]> result = new ScaffoldResponse();
        QueryDescriptor<T> de = service.useQuery().select(c);
        if (se != null) {
            StrawCriteria cr = se.search();
            if (cr != null) {
                de = de.where(cr);
            }
        }
        result.setData((T[]) de.orderBy("asc", "id").execute());
        return result;
    }

    @PostMapping(value = "/all-pdf", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> listeAllpdf(@RequestHeader("Authorization") String token, @RequestBody(required = false) Se se, HttpServletResponse http) throws Exception {
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        QueryDescriptor<T> de = service.useQuery().select(c);
        System.out.println("se " + se);
        if (se != null) {
            StrawCriteria cr = se.search();
            if (cr != null) {
                de = de.where(cr);
            }
        }
        T[] liste = (T[]) de.orderBy("asc", "id").execute();
        Reflection s = new Reflection(c);
        Set<String> aa = s.getFields().keySet();
        JVEList<T> jve = new JVEList(liste, aa.toArray(new String[aa.size()]));
        ScMarker mk = new ScMarker("liste.ftl");
        mk.put("titre", "list of " + c.getSimpleName());
        Table table = jve.genHTML();
        table.addClasse("table-striped");
        table.get(0).wrapIn(new HTMLtag("thead"));
        mk.put("table", table.getTag());
        System.out.println(mk.getContent());
        http.setContentType("application/pdf");
        try {
            http.setHeader("Content-Disposition", "attachement; filename=" + c.getSimpleName() + ".pdf");
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
                    .body(ScHandler.toPdf(mk.getContent(), c.getSimpleName() + ".pdf"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping("/count")
    public StatuedResponse total(@RequestHeader("Authorization") String token, @RequestBody(required = false) Se se) throws Exception {
//        System.out.println("token :"+tokenable.isExpired(bearer(token)));
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        StatuedResponse retour = new StatuedResponse();
        String[] fields = ScHandler.getShows(c);

        QueryDescriptor<T> de = service.useQuery().select(c);
        if (fields != null) {
            de = service.useQuery().select(c, fields);
        }
        if (se != null) {

            StrawCriteria cr = se.search();
            if (cr != null) {
                de = de.where(cr);
            }
        }
        T[] a = de.execute();
        System.out.println("Mndeha le Count " + a.length);
        retour.setData(a.length);
        return retour;
    }

//
//    public ScaffoldResponse<T[]> search() throws Exception {
//     
//        StrawCriteria a = StrawSession.createStrawCriteria(loader.getObject());
//        return null;
//    }
//
//    @ResponseBody
    @GetMapping("/loadsearch")
    public StatuedResponse initsearch(@RequestHeader("Authorization") String token) throws Exception {

        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        StatuedResponse retour = new StatuedResponse();
        try {
            ArrayList<ScDataSearch> toload = ScHandler.loadDependency(search);
            retour.setData(toload);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ScaffoldController.class.getName()).log(Level.SEVERE, null, ex);
            retour.setMessage(ex.getMessage());
            retour.setStatus(-1);
        } catch (Exception ex) {
            retour.setMessage(ex.getMessage());
            retour.setStatus(-1);
        }

        return retour;
    }

    @GetMapping("/loadsave")
    public StatuedResponse initsave(@RequestHeader("Authorization") String token) throws Exception {

        StatuedResponse retour = new StatuedResponse();
        Object load;
        try {
            boolean teste = tokenable.isExpired(bearer(token));
            if (teste) {
                retour.setMessage("Acess Denied");
                retour.setStatus(-1);
            }
            load = c.getConstructor().newInstance();
            ArrayList<ScData> toload = ScHandler.loadDependency(load);
            retour.setData(toload);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(ScaffoldController.class.getName()).log(Level.SEVERE, null, ex);
            retour.setMessage(ex.getMessage());
            retour.setStatus(-1);
        } catch (Exception ex) {
            retour.setMessage(ex.getMessage());
            retour.setStatus(-1);
        }

        return retour;
    }
//

    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    @PostMapping("/sv")
    public StatuedResponse executesave(@RequestHeader("Authorization") String token, @RequestBody T object) throws Exception {
        System.out.println("tafiditra");
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        StatuedResponse retour = new StatuedResponse();
        try {
            service.save(object);
            retour.setData(object);
            retour.setStatus(0);
            retour.setMessage("Saved!!");
        } catch (Exception ex) {

            retour.setStatus(-1);
            retour.setMessage(ex.getMessage());
            throw ex;
        }
        return retour;
    }
//

    @ResponseBody
    @PostMapping("/up")
    public StatuedResponse executeupdate(@RequestHeader("Authorization") String token, @RequestBody Wrapper<T> wrapper) throws Exception {
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        StatuedResponse retour = new StatuedResponse();
        T old = wrapper.getOld();
        T brand = wrapper.getBrand();
//        System.out.println(old.getClass()+"="+brand.getClass());
        try {
            service.update(old, brand);
            retour.setStatus(0);
            retour.setMessage("Successfully Updated!!");
        } catch (Exception ex) {

            retour.setStatus(-1);
            retour.setMessage(ex.getMessage());
        }

        return retour;
    }

    @ResponseBody
    @PostMapping("/delete")
    public StatuedResponse executedelete(@RequestHeader("Authorization") String token, @RequestBody T object) throws Exception {
        boolean teste = tokenable.isExpired(bearer(token));
        if (teste) {
            throw new Exception("Acess Denied");
        }
        StatuedResponse retour = new StatuedResponse();
        try {
            service.delete(object);
            retour.setMessage("Deleted successfully :(");
            retour.setStatus(0);
        } catch (Exception ex) {
            retour.setStatus(-1);
            retour.setMessage(ex.getMessage());
        }
        return retour;
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        try {
            List<T> fileData = readFileData(file);
            for(T i:fileData){
                System.out.println(i.toString());
                service.save(i);
            }
            // Process the file data as needed (e.g., convert to JSON, perform calculations)

            return ResponseEntity.ok("File uploaded successfully");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to read the file");
        }
    }

    private List<T> readFileData(MultipartFile file) throws IOException, Exception {
        List<String> fileData = new ArrayList<>();
        List<T> retour=new ArrayList<>();
        try (InputStream inputStream = file.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            // Skip the BOM if present
            reader.mark(1);
            if (reader.read() != 0xFEFF) {
                reader.reset();
            }

            CSVParser csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);
            for (CSVRecord record : csvParser) {
                T temp=c.getConstructor().newInstance();
                record.toList().forEach((i)->{ System.out.println("->"+i); });
                record.toMap().forEach((i,j)->{ System.out.println(i+":"+j); });
                
                if(temp instanceof StringParser){
                    StringParser p=(StringParser)temp;
                    p.parse(record.toMap());
                    temp=(T)p;
                }
                System.out.println("ioooo->"+temp.toString());
                retour.add((T)temp);
                // Access CSV data using column names or indexes
//                String id = record.get("id");
//                String vehiculeid = record.get("vehiculeid");
//                String numero = record.get("numero");
//                String idmodele = record.get("idmodele");

                // Process the data or add to the result list
                // ...
                fileData.add(record.toString());
            }
        }

        return retour;
    }
}
