/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.contrat;

import scaffold.contrat.interfaces.Depense;
import scaffold.contrat.interfaces.Recette;

/**
 *
 * @author andri
 */
public class BeneficeNet {
    public Double getBeneficeNet(Recette benefice,Depense depense) throws Exception{
        return benefice.getRecette()-depense.getDepense();
    } 
}
