/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.handler;

import com.itextpdf.html2pdf.HtmlConverter;
import com.reflect.Reflection;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Search;
import scaffold.annotation.Show;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ScHandler {

    public static Object[] loadDatas(Field f) throws Exception {
        if (f.isAnnotationPresent(LoadData.class)) {
            String fullName = f.getAnnotation(LoadData.class).value();
            if (!fullName.isEmpty()) {
                int lastDotIndex = fullName.lastIndexOf(".");
                String method = fullName.substring(lastDotIndex + 1);
                String classeName = fullName.substring(0, lastDotIndex);
                Class<?> toclass = Class.forName(classeName);
                Method m = toclass.getMethod(method);
                return (Object[]) m.invoke(toclass.getConstructor().newInstance());
            }
        }

        return null;
    }

    public static LoadData getLoad(Field f) {
        if (f.isAnnotationPresent(LoadData.class)) {
            return f.getAnnotation(LoadData.class);
        }
        return null;

    }

    static FieldLoad loadField(Field f) {
        Class fa = FieldLoad.class;
        if (f.isAnnotationPresent(fa)) {
            return (FieldLoad) f.getAnnotation(fa);
        }
        return null;
    }

    public static ArrayList<ScDataSearch> loadDependency(SearchTools object) throws Exception {
        Reflection reflection = new Reflection(object.getClass());
        Collection<Field> fields = reflection.getFields().values();
        ArrayList<ScDataSearch> retour = new ArrayList<>();
        for (Field f : fields) {
            ScDataSearch temp = new ScDataSearch();
            loadDataForInputs(temp, f);
            typesearch(temp, f);
            loadFieldForInputs(retour, temp, f);
        }
        return retour;
    }
    
    public static void typesearch(ScDataSearch t,Field f){
        if(SearchTools.isArray(f)){
            t.setTypesearch("minmax");
            
        }
    }

    public static void loadDataForInputs(ScData t, Field f) throws Exception {
        Object[] datas = loadDatas(f);
        if (datas != null) {
            t.setDatas(datas);
            LoadData loader = f.getAnnotation(LoadData.class);
            t.setDatalabelkey(loader.datalabelkey());
            t.setDatanamekey(loader.datanamekey());
        }
    }
    public  static <T extends ScData> void loadFieldForInputs(ArrayList<T> retour,T t,Field f){
        FieldLoad loads = loadField(f);
            if (loads != null) {
                String name = (loads.fieldname().isEmpty()) ? f.getName() : loads.fieldname();
                String liste = (loads.list().isEmpty()) ? null : loads.list();
                String type = (loads.type().isEmpty()) ? null : loads.type();
                ScField sc = new ScField();
                sc.setFieldname(name);
                t.setLabel(name);
                sc.setList(liste);
                sc.setType(type);
                t.setFields(sc);
                if(t instanceof ScDataSearch){
                     if(loads.type().equals("arrayOption") || !loads.list().isEmpty())
                    ((ScDataSearch)t).setTypesearch("arrayOption");
                }
                retour.add(t);
            }
    }

    public static ArrayList<ScData> loadDependency(Object object) throws Exception {
        Reflection reflection = new Reflection(object.getClass());
        Collection<Field> fields = reflection.getFields().values();
        ArrayList<ScData> retour = new ArrayList<>();
        for (Field f : fields) {
            ScData temp = new ScData();
            //load data
            loadDataForInputs(temp, f);

            //load field
            loadFieldForInputs(retour,temp,f);
        }
        return retour;
    }
    
    public static  void convert(String htmlPath, String pdfPath) throws IOException {
        HtmlConverter.convertToPdf(new File(htmlPath), new File(pdfPath));
    }

    public static byte[] toPdf(String html, String pdfPath) throws IOException {
        try (FileOutputStream pdf = new FileOutputStream(pdfPath)) {
            HtmlConverter.convertToPdf(html, pdf);
        }
        return Files.readAllBytes(Paths.get(pdfPath));
    }
    
    public static String[] getShows(Class obj){
        if(obj.isAnnotationPresent(Show.class)){
            Show a=(Show) obj.getAnnotation(Show.class);
            return a.values();
        }
        return null;
    }
    
//    public static String toTimestamp_03(Timestamp)
}
