/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.handler;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateNotFoundException;
import java.io.IOException;

/**
 *
 * @author andri
 */
public class ScMarker {
     static Configuration configuration;

    static {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
//        cfg.setDirectoryForTemplateLoading(new File("src/main/resources/templates"));
        cfg.setClassForTemplateLoading(ScMarker.class, "/templates");
        cfg.setDefaultEncoding("UTF-8");
        ScMarker.configuration=cfg;
    }
    public static Template getTemplate(String model)
            throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {
        return ScMarker.configuration.getTemplate(model);
    }
}
