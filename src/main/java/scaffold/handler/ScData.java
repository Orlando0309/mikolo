/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.handler;

/**
 *
 * @author andri
 */
public class ScData {
    String label;
    Object[] datas=new Object[0];
    ScField fields=new ScField();
    String datalabelkey="null",datanamekey="null";

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object[] getDatas() {
        return datas;
    }

    public void setDatas(Object[] datas) {
        this.datas = datas;
    }

    public ScField getFields() {
        return fields;
    }

    public void setFields(ScField fields) {
        this.fields = fields;
    }

    public String getDatalabelkey() {
        return datalabelkey;
    }

    public void setDatalabelkey(String datalabelkey) {
        this.datalabelkey = datalabelkey;
    }

    public String getDatanamekey() {
        return datanamekey;
    }

    public void setDatanamekey(String datanamekey) {
        this.datanamekey = datanamekey;
    }
    
    
    
}
