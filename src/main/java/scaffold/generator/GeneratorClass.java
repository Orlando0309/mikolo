/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.generator;

/**
 *
 * @author andri
 */
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GeneratorClass {
    private final Connection connection;

    public GeneratorClass(Connection connection) {
        this.connection = connection;
    }
    
    public boolean isPrimaryKey(String tableName, String columnName) throws SQLException {
        DatabaseMetaData metadata = connection.getMetaData();
        ResultSet resultSet = metadata.getPrimaryKeys(null, null, tableName);

        while (resultSet.next()) {
            String pkColumnName = resultSet.getString("COLUMN_NAME");
            if (pkColumnName.equals(columnName)) {
                return true;
            }
        }

        return false;
    }

    public Map<String, Mapper> Columns(String table) throws SQLException {
        Map<String, Mapper> columnMap = new HashMap<>();
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet resultSet = metaData.getColumns(null, null, table, null);
        System.out.println("Columns------------------------------");
        while (resultSet.next()) {
            String columnName = resultSet.getString("COLUMN_NAME");
            String columnType = resultSet.getString("TYPE_NAME");
            System.out.println("c:"+columnName+" "+columnType);
            
            String javaType = mapToJavaType(columnType);
            System.out.println("Colt:"+columnType+":"+javaType);
            Mapper map=new Mapper(isPrimaryKey(table, columnName), isForeignKey(table, columnName), javaType,LoadToJavaType(columnType));
            columnMap.put(columnName, map);
        }

        return columnMap;
    }
    
     public boolean isForeignKey(String tableName, String columnName) throws SQLException {
        DatabaseMetaData metadata = connection.getMetaData();
        ResultSet resultSet = metadata.getImportedKeys(null, null, tableName);

        while (resultSet.next()) {
            String fkColumnName = resultSet.getString("FKCOLUMN_NAME");
            if (fkColumnName.equals(columnName)) {
                return true;
            }
        }

        return false;
    }

    public String getReferencedTable(String tableName, String columnName) throws SQLException {
        DatabaseMetaData metadata = connection.getMetaData();
        ResultSet resultSet = metadata.getImportedKeys(null, null, tableName);

        while (resultSet.next()) {
            String fkColumnName = resultSet.getString("FKCOLUMN_NAME");
            if (fkColumnName.equals(columnName)) {
                return resultSet.getString("PKTABLE_NAME");
            }
        }

        return "";
    }

    public void generateClassFile(String packageName, String className, String content) throws IOException {
        String packagePath = packageName.replace(".", "/");
        String filePath = String.format("src/main/java/%s/%s.java", packagePath, className);
        
        FileWriter fileWriter = new FileWriter(filePath);
        fileWriter.write(content);
        fileWriter.close();
        
        System.out.println("Class file generated successfully.");
    }
    
      private String LoadToJavaType(String columnType) {
        switch (columnType.toLowerCase()) {
            case "int":
            case "integer":
            case "serial":
            case "int4":
                return "number";
            case "varchar":
            case "nvarchar":
            case "char":
            case "text":
                return "text";
            case "date":
                return "date";
            case "datetime":
            case "timestamptz":
            case "timestampt":
                return "datetime-local";
            case "float8":
                return "number";
            // Add more mappings for other column types as needed
            default:
                return "text";
        }
    }
    private String mapToJavaType(String columnType) {
        switch (columnType.toLowerCase()) {
            case "int":
            case "integer":
            case "serial":
            case "int4":
                return "Integer";
            case "varchar":
            case "nvarchar":
            case "char":
            case "text":
                return "String";
            case "date":
            case "datetime":
                return "java.util.Date";
            case "timestamptz":
            case "timestampt":
                return "java.sql.Timestamp";
            case "float8":
                return "Double";
            // Add more mappings for other column types as needed
            default:
                return "Object";
        }
    }
    public String importget() throws SQLException {
        DatabaseMetaData metaData = connection.getMetaData();
        ResultSet resultSet = metaData.getColumns(null, null, null, null);

        while (resultSet.next()) {
            String columnType = resultSet.getString("TYPE_NAME");
            if (columnType.equals(mapToJavaType(columnType))) {
                return "import java.util.Date;";
            }
        }

        return "";
    }

    public String className(String table) {
        
        String[] parts = table.split("~");
        StringBuilder classNameBuilder = new StringBuilder();

        for (String part : parts) {
            classNameBuilder.append(Character.toUpperCase(part.charAt(0))).append(part.substring(1));
        }

        return classNameBuilder.toString();
    }
}
