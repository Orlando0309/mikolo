/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.generator;

/**
 *
 * @author andri
 */
public class Mapper {
        boolean isprimary;
        boolean isforeign;
        String javatype;
        String loadtype;

    public Mapper(boolean isprimary, boolean isforeign, String javatype,String loadtype) {
        this.isprimary = isprimary;
        this.isforeign = isforeign;
        this.javatype = javatype;
        this.loadtype=loadtype;
    }

        
    public Mapper() {
    }

    public String getLoadtype() {
        return loadtype;
    }

    public void setLoadtype(String loadtype) {
        this.loadtype = loadtype;
    }
    
    
        

    public boolean isIsprimary() {
        return isprimary;
    }

    public void setIsprimary(boolean isprimary) {
        this.isprimary = isprimary;
    }

    public boolean isIsforeign() {
        return isforeign;
    }

    public void setIsforeign(boolean isforeign) {
        this.isforeign = isforeign;
    }

    public String getJavatype() {
        return javatype;
    }

    public void setJavatype(String javatype) {
        this.javatype = javatype;
    }
        
}
