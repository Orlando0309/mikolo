/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import prestation.RecetteHelper;
import scaffold.chart.ChartData;
import service.ServiceV_recette;

/**
 *
 * @author andri
 */
@RequestMapping("/chart")
@Component
@RestController
@CrossOrigin(value = "*")
public class ChartController {
    
        @PostMapping
    public ChartData get() throws Exception{
            ServiceV_recette re=new ServiceV_recette();
            RecetteHelper help=new RecetteHelper(re.list());
            return help.toDatasets();
    }
}
