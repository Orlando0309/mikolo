/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold;

import com.strawberry.session.StrawData;
import com.strawberry.session.StrawSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import scaffold.generator.GeneratorClass;
import terminal.Terminal;

/**
 *
 * @author andri
 */
@RequestMapping("/generate")
@Component
@RestController
@CrossOrigin(value = "*")
public class GenerateController {

    private final ResourceLoader resourceLoader;

    public GenerateController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void init() {
        Resource resource = resourceLoader.getResource("classpath:straw.xml");
        try {
            String path = resource.getFile().getPath();
            StrawData.init(path);
        } catch (IOException ex) {
            Logger.getLogger(ScaffoldController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @ResponseBody
    @GetMapping("teste")
    public String mande(){
        return "mandeha";
    }
    
    @PostMapping
    public String generate(@RequestBody GeneratorMapping generator) throws Exception {
        String table = generator.getTable(), packages = generator.getPackages(), servicepackage = generator.getServicepackage(), modele = generator.getModele();
        System.out.println("asooo");
        StrawData.initialise();
        StrawSession con = StrawSession.session;
        Connection cona = con.getConnection();
        GeneratorClass gen = new GeneratorClass(cona);
        gen.Columns(table);
        String t = gen.className(modele);

        String content = generateModele(generator, gen, modele, packages);

        String contents = generateService(servicepackage, packages, t, gen);
        generateSearch(generator, t, gen);
        generateController(generator, gen);

        return content;
    }

    public static String generateModele(GeneratorMapping generator, GeneratorClass gen, String modele, String packages) throws Exception {
        ScMarker marker = new ScMarker("mappings.ftl");
//     Generation modele
        String t = gen.className(modele);
        marker.put("tableName", generator.getTable());
        marker.put("mod", modele);
        marker.put("generator", gen);
        marker.put("packages", packages);
        marker.put("loads", true);
        String retour = marker.getContent();
        gen.generateClassFile(packages, t, retour);
        return retour;
    }

    public static void generateSearch(GeneratorMapping generator, String t, GeneratorClass gen) throws Exception {
        if (!generator.getPackagesearch().isEmpty()) {
            ScMarker search = new ScMarker("search.ftl");
            search.put("packageclass", generator.getPackagesearch());
            search.put("model", t);
//        gen.generateClassFile(generator.getPackagesearch(), content, content);
            gen.generateClassFile(generator.getPackagesearch(), String.format("%sSearch", Terminal.ucfirst(t, 0)), search.getContent());

        }

    }

    public static String generateService(String servicepackage, String packages, String t, GeneratorClass gen) throws Exception {
        ScMarker marker = new ScMarker();
        marker.setModel("service.ftl");
        marker.put("packageservice", servicepackage);
        marker.put("packageclass", packages);
        marker.put("model", t);

        String contents = marker.getContent();
        gen.generateClassFile(servicepackage, String.format("Service%s", Terminal.ucfirst(t, 0)), contents);
        return contents;
    }

    public static void generateController(GeneratorMapping gen, GeneratorClass gen2) throws Exception {
        if (!gen.getPackagecontroller().isEmpty() && !gen.getRouteController().isEmpty()) {
            ScMarker marker = new ScMarker();
            String t = gen2.className(gen.getModele());
            marker.setModel("controller.ftl");
            marker.put("controller", gen.getPackagecontroller());
            marker.put("model", t);
            marker.put("packagesearch", gen.getPackagesearch());
            marker.put("packageclass", gen.getPackages());
            marker.put("tokenable", gen.getTokenable());
            marker.put("packageservice", gen.getServicepackage());
            marker.put("url", gen.getRouteController());
            String contents = marker.getContent();
            gen2.generateClassFile(gen.getPackagecontroller(), String.format("%sController", Terminal.ucfirst(t, 0)), contents);
        }

    }
}
