/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.chart;

/**
 *
 * @author andri
 */
public class ChartData {
    String[] labels;
    Datasets[] datasets;

    public ChartData(String[] labels, Datasets... datasets) {
        this.labels = labels;
        this.datasets = datasets;
    }

    public ChartData() {
    }

    
    
    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public Datasets[] getDatasets() {
        return datasets;
    }

    public void setDatasets(Datasets... datasets) {
        this.datasets = datasets;
    }
    
}
