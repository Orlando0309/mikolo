/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scaffold.chart;

/**
 *
 * @author andri
 */
public class Datasets {
    String label;
    Object[] data;
    String[] backgroundColor;
    String borderColor;
    Integer borderWidth;

    public Datasets(String label, Object[] data, String[] backgroundColor, String borderColor, Integer borderWidth) {
        this.label = label;
        this.data = data;
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
        this.borderWidth = borderWidth;
    }
    

    public Datasets(String label, Object[] data, String[] backgroundColor) {
        this.label = label;
        this.data = data;
        this.backgroundColor = backgroundColor;
    }

    public Datasets() {
    }
    
    
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object[] getData() {
        return data;
    }

    public void setData(Object... data) {
        this.data = data;
    }

    public String[] getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String... backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }
    
}
