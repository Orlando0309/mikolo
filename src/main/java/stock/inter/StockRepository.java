/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stock.inter;

/**
 *
 * @author andri
 * @param <T>
 */
public interface StockRepository<T extends Object> {
    public void addToStock(T object) throws Exception;
    public void removeFromStock(T object) throws Exception;
    public double totalInStock(T object) throws Exception;
}
