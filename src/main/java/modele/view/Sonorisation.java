package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
import constante.ConstanteEvent;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Show;


@TableName("sonoristations")
@Show(values = {"id","nomelement","prix"})
public class Sonorisation {
    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
            @LoadData(value = "service.ServiceTypeabonnement.list",datalabelkey = "nom",datanamekey = "id")
    Integer typeabn;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

//    @ColumnName(nullable = true)
    
//            @FieldLoad(type = "number")
//    Integer place;

    @ColumnName
    
//            @FieldLoad(type = "arrayOption")
    Integer prestation=ConstanteEvent.SONO;

    @ColumnName
    
//            @FieldLoad(type = "arrayOption")
//            @LoadData(value = "service.ServiceTypetarif.list",datalabelkey = "nomtarif",datanamekey = "id")
    Integer typetarif=ConstanteEvent.PAR_HEURE;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nomelement;


    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getTypeabn() {
        return typeabn;
    }

    public void setTypeabn(Integer typeabn) {
        this.typeabn = typeabn;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
//    public Integer getPlace() {
//        return place;
//    }
//
//    public void setPlace(Integer place) {
//        this.place = place;
//    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public Integer getTypetarif() {
        return typetarif;
    }

    public void setTypetarif(Integer typetarif) {
        this.typetarif = typetarif;
    }
    public String getNomelement() {
        return nomelement;
    }

    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }
}
