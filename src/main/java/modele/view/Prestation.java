package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName
public class Prestation {
    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nom;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer fixe;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public Integer getFixe() {
        return fixe;
    }

    public void setFixe(Integer fixe) {
        this.fixe = fixe;
    }
}
