package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Show;


@TableName("v_devis_evenement")
@Show(values = {"devis_id","evenement_nom","nomevenement","devis_daty"})
public class V_Devis {
    @ColumnName
    
            @FieldLoad(type = "text")
    String evenement_nom;
    
    @ColumnName
    String nomevenement;

    @ColumnName(nullable = true)
    
            @FieldLoad(type = "text")
    String devis_ref;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer evenement_id;

    @ColumnName
    
            @FieldLoad(type = "datetime-local")
    java.sql.Timestamp devis_daty;

    @ColumnName("id")
    
            @FieldLoad(type = "number")
    Integer devis_id;

    public String getNomevenement() {
        return nomevenement;
    }

    public void setNomevenement(String nomevenement) {
        this.nomevenement = nomevenement;
    }


    
    public String getEvenement_nom() {
        return evenement_nom;
    }

    public void setEvenement_nom(String evenement_nom) {
        this.evenement_nom = evenement_nom;
    }
    public String getDevis_ref() {
        return devis_ref;
    }

    public void setDevis_ref(String devis_ref) {
        this.devis_ref = devis_ref;
    }
    public Integer getEvenement_id() {
        return evenement_id;
    }

    public void setEvenement_id(Integer evenement_id) {
        this.evenement_id = evenement_id;
    }
    public java.sql.Timestamp getDevis_daty() {
        return devis_daty;
    }

    public void setDevis_daty(java.sql.Timestamp devis_daty) {
        this.devis_daty = devis_daty;
    }
    public Integer getDevis_id() {
        return devis_id;
    }

    public void setDevis_id(Integer devis_id) {
        this.devis_id = devis_id;
    }
}
