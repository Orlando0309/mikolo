package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("typelieu")
public class Typelieu {
    @ColumnName
    
            @FieldLoad(type = "text")
    String nomtypelieu;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;


    public String getNomtypelieu() {
        return nomtypelieu;
    }

    public void setNomtypelieu(String nomtypelieu) {
        this.nomtypelieu = nomtypelieu;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
