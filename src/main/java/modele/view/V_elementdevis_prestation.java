package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("v_elementdevis_prestation")

public class V_elementdevis_prestation {
    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer prestation_id;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
              @FieldLoad(type = "arrayOption")
            @LoadData(value = "service.ServiceElementprestation.list",datanamekey = "id",datalabelkey = "nomelement")
    Integer elementprestation_id;
    @ColumnName
    
            @FieldLoad(type = "number")
    Double duree;

    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer id;

    @ColumnName
    
//            @FieldLoad(type = "text")
    String prestation;

    @ColumnName
    
//            @FieldLoad(type = "text")
    String nomelement;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
             @LoadData(value = "service.ServiceDevis.list",datanamekey = "id",datalabelkey = "nomevenement")
    Integer devis;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer quantite;


    public Integer getPrestation_id() {
        return prestation_id;
    }

    public void setPrestation_id(Integer prestation_id) {
        this.prestation_id = prestation_id;
    }
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getElementprestation_id() {
        return elementprestation_id;
    }

    public void setElementprestation_id(Integer elementprestation_id) {
        this.elementprestation_id = elementprestation_id;
    }
    public Double getDuree() {
        return duree;
    }

    public void setDuree(Double duree) {
        this.duree = duree;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getPrestation() {
        return prestation;
    }

    public void setPrestation(String prestation) {
        this.prestation = prestation;
    }
    public String getNomelement() {
        return nomelement;
    }

    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }
}
