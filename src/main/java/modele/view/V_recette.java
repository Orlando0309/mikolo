package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("v_recette")
public class V_recette {
    @ColumnName
    
            @FieldLoad(type = "number")
    Double recette;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer prestation;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nom;


    public Double getRecette() {
        return recette;
    }

    public void setRecette(Double recette) {
        this.recette = recette;
    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
