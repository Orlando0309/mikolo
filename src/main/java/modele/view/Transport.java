package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
import constante.ConstanteEvent;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("elementprestation")
public class Transport {
     @ColumnName(nullable = true)
    
//            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
//            @FieldLoad(type = "arrayOption")
    Integer typeabn=ConstanteEvent.STANDARD;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName(nullable = true)
    
//            @FieldLoad(type = "number")
    Integer place;

    @ColumnName
    
//            @FieldLoad(type = "arrayOption")
    Integer prestation=ConstanteEvent.TRANS;

    @ColumnName
    
//            @FieldLoad(type = "arrayOption")
    Integer typetarif=ConstanteEvent.STANDARD;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nomelement;

    
            @ColumnName
    
//            @FieldLoad(type = "file")
    String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getTypeabn() {
        return typeabn;
    }

    public void setTypeabn(Integer typeabn) {
        this.typeabn = typeabn;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public Integer getTypetarif() {
        return typetarif;
    }

    public void setTypetarif(Integer typetarif) {
        this.typetarif = typetarif;
    }
    public String getNomelement() {
        return nomelement;
    }

    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }
    
}
