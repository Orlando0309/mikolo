package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("v_artiste_spectacle")
public class V_artiste_spectacle {
    @ColumnName
    
            @FieldLoad(type = "text")
    String nomartiste;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer valeurprestation;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double duree;

    @ColumnName
    
            @FieldLoad(type = "text")
    String photo;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer prestation;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer devis;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer quantite;


    public String getNomartiste() {
        return nomartiste;
    }

    public void setNomartiste(String nomartiste) {
        this.nomartiste = nomartiste;
    }
    public Integer getValeurprestation() {
        return valeurprestation;
    }

    public void setValeurprestation(Integer valeurprestation) {
        this.valeurprestation = valeurprestation;
    }
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Double getDuree() {
        return duree;
    }

    public void setDuree(Double duree) {
        this.duree = duree;
    }
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }
}
