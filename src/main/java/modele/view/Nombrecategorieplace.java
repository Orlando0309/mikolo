package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("nombrecategorieplace")
public class Nombrecategorieplace {
    @ColumnName
    
            @FieldLoad(type = "number")
    Integer nb;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
            @LoadData(value = "service.ServiceLieux.list",datalabelkey = "nomelement",datanamekey = "id")
    Integer idlieu;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @LoadData(value = "service.ServiceCategoriePlace.list",datalabelkey = "nom",datanamekey = "id")
            @FieldLoad(type = "arrayOption")
    Integer idcategorieplace;


    public Integer getNb() {
        return nb;
    }

    public void setNb(Integer nb) {
        this.nb = nb;
    }
    public Integer getIdlieu() {
        return idlieu;
    }

    public void setIdlieu(Integer idlieu) {
        this.idlieu = idlieu;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getIdcategorieplace() {
        return idcategorieplace;
    }

    public void setIdcategorieplace(Integer idcategorieplace) {
        this.idcategorieplace = idcategorieplace;
    }
}
