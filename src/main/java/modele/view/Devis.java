package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("devis")
public class Devis {
    @ColumnName(insertable = false, hasdefault = true)
    
//            @FieldLoad(type = "text")
    String ref;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "datetime-local")
    java.sql.Timestamp daty;
    
    @ColumnName
            @FieldLoad(type = "arrayOption")
            @LoadData(value ="service.ServiceEvenement.list",datanamekey = "id",datalabelkey = "nom" )
    Integer evenement;
    @ColumnName
            @FieldLoad(type = "text")
    String nomevenement;

    public Integer getEvenement() {
        return evenement;
    }

    public void setEvenement(Integer evenement) {
        this.evenement = evenement;
    }

    public String getNomevenement() {
        return nomevenement;
    }

    public void setNomevenement(String nomevenement) {
        this.nomevenement = nomevenement;
    }

  
    

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public java.sql.Timestamp getDaty() {
        return daty;
    }

    public void setDaty(java.sql.Timestamp daty) {
        this.daty = daty;
    }
}
