package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("typetarif")
public class Typetarif {
    @ColumnName
    
            @FieldLoad(type = "text")
    String nomtarif;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;


    public String getNomtarif() {
        return nomtarif;
    }

    public void setNomtarif(String nomtarif) {
        this.nomtarif = nomtarif;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
