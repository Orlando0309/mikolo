package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Show;

@Show(values = {"nom","prix","devis"})
@TableName("v_prixdevis")
public class V_prixdevis {
    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer categorieplace;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nom;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer devis;


    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getCategorieplace() {
        return categorieplace;
    }

    public void setCategorieplace(Integer categorieplace) {
        this.categorieplace = categorieplace;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
}
