package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("elementdevis")
public class Elementdevis {
    @ColumnName
    
            @FieldLoad(type = "arrayOption")
              @LoadData(value = "service.ServiceElementprestation.list",datanamekey = "id",datalabelkey = "nomelement")
    Integer valeurprestation;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double duree;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
    Integer prestation;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
                        @LoadData(value = "service.ServiceDevis.list",datanamekey = "id",datalabelkey = "nomevenement")
    Integer devis;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer quantite;


    public Integer getValeurprestation() {
        return valeurprestation;
    }

    public void setValeurprestation(Integer valeurprestation) {
        this.valeurprestation = valeurprestation;
    }
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Double getDuree() {
        return duree;
    }

    public void setDuree(Double duree) {
        this.duree = duree;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }
}
