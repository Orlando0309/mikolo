package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Crypt;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
import com.strawberry.auth.Identifiant;
import com.strawberry.auth.Password;
import com.strawberry.auth.impl.TokenRoot;
import com.strawberry.token.TokenGenerator;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName
public class Employe implements TokenRoot  {
    @ColumnName
    
            @FieldLoad(type = "text")
            @Password
            @Crypt
    String password;

    @ColumnName
    @Identifiant
            @FieldLoad(type = "text")
    String identifiant;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "date")
    java.sql.Timestamp expired_at;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nom;

    @ColumnName
    
            @FieldLoad(type = "text")
    String prenom;

    @ColumnName
    
            @FieldLoad(type = "text")
    String token;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public java.sql.Timestamp getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(java.sql.Timestamp expired_at) {
        this.expired_at = expired_at;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Integer getTimeExpiration() {
        return 3600;
    }

    @Override
    public void action(String string) throws Exception {
        this.password = null;
        Date date = TokenGenerator.expirationTime(this.getTimeExpiration());

        if (string.equals("auth")) {
            Timestamp timestamp = new Timestamp(date.getTime());

// Create a SimpleDateFormat instance with GMT+3 time zone
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+3")); // or sdf.setTimeZone(TimeZone.getTimeZone("Etc/GMT+3"))

// Format the date with the specified time zone
            String formattedDate = sdf.format(timestamp);
            this.setExpired_at(Timestamp.valueOf(formattedDate));
            this.setToken(TokenGenerator.generateToken(id.toString()));
        }
    }
    
}
