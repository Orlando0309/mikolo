package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("v_prestation_abonnement")
public class PrestationComplet {
    @ColumnName
    
            @FieldLoad(type = "text")
    String typeabonnement_nom;

    @ColumnName
    
            @FieldLoad(type = "text")
    String elementprestation_nomelement;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer prestation_id;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nomtarif;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer elementprestation_id;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer prestation_fixe;

    @ColumnName
    
            @FieldLoad(type = "text")
    String prestation_nom;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer elementprestation_typetarif;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double elementprestation_prix;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer elementprestation_place;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer elementprestation_typeabn;


    public String getTypeabonnement_nom() {
        return typeabonnement_nom;
    }

    public void setTypeabonnement_nom(String typeabonnement_nom) {
        this.typeabonnement_nom = typeabonnement_nom;
    }
    public String getElementprestation_nomelement() {
        return elementprestation_nomelement;
    }

    public void setElementprestation_nomelement(String elementprestation_nomelement) {
        this.elementprestation_nomelement = elementprestation_nomelement;
    }
    public Integer getPrestation_id() {
        return prestation_id;
    }

    public void setPrestation_id(Integer prestation_id) {
        this.prestation_id = prestation_id;
    }
    public String getNomtarif() {
        return nomtarif;
    }

    public void setNomtarif(String nomtarif) {
        this.nomtarif = nomtarif;
    }
    public Integer getElementprestation_id() {
        return elementprestation_id;
    }

    public void setElementprestation_id(Integer elementprestation_id) {
        this.elementprestation_id = elementprestation_id;
    }
    public Integer getPrestation_fixe() {
        return prestation_fixe;
    }

    public void setPrestation_fixe(Integer prestation_fixe) {
        this.prestation_fixe = prestation_fixe;
    }
    public String getPrestation_nom() {
        return prestation_nom;
    }

    public void setPrestation_nom(String prestation_nom) {
        this.prestation_nom = prestation_nom;
    }
    public Integer getElementprestation_typetarif() {
        return elementprestation_typetarif;
    }

    public void setElementprestation_typetarif(Integer elementprestation_typetarif) {
        this.elementprestation_typetarif = elementprestation_typetarif;
    }
    public Double getElementprestation_prix() {
        return elementprestation_prix;
    }

    public void setElementprestation_prix(Double elementprestation_prix) {
        this.elementprestation_prix = elementprestation_prix;
    }
    public Integer getElementprestation_place() {
        return elementprestation_place;
    }

    public void setElementprestation_place(Integer elementprestation_place) {
        this.elementprestation_place = elementprestation_place;
    }
    public Integer getElementprestation_typeabn() {
        return elementprestation_typeabn;
    }

    public void setElementprestation_typeabn(Integer elementprestation_typeabn) {
        this.elementprestation_typeabn = elementprestation_typeabn;
    }
}
