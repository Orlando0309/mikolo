package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Show;


@TableName("v_elementdevis_prestation_devis")
@Show(values = {"nomevenement","prestation","nomelement"})
public class V_elementdevis_prestation_devis extends V_elementdevis_prestation {

    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer devis_id;

   
     @ColumnName
    String nomevenement;

    public String getNomevenement() {
        return nomevenement;
    }

    public void setNomevenement(String nomevenement) {
        this.nomevenement = nomevenement;
    }
    
    public Integer getDevis_id() {
        return devis_id;
    }

    public void setDevis_id(Integer devis_id) {
        this.devis_id = devis_id;
    }
}
