package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("placevendu")
public class Placevendu {
    @ColumnName
    
            @FieldLoad(type = "number")
    Integer nombrevendu;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
    Integer categorieplace;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
    Integer devis;


    public Integer getNombrevendu() {
        return nombrevendu;
    }

    public void setNombrevendu(Integer nombrevendu) {
        this.nombrevendu = nombrevendu;
    }
    public Integer getCategorieplace() {
        return categorieplace;
    }

    public void setCategorieplace(Integer categorieplace) {
        this.categorieplace = categorieplace;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
}
