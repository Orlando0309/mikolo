package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("lieu")
public class Lieu {
    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
@LoadData(value = "service.ServiceTypelieu.list",datanamekey = "id",datalabelkey = "nomtypelieu")    
            @FieldLoad(type = "arrayOption")
    Integer typelieu;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nom;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer maxplace;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getTypelieu() {
        return typelieu;
    }

    public void setTypelieu(Integer typelieu) {
        this.typelieu = typelieu;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public Integer getMaxplace() {
        return maxplace;
    }

    public void setMaxplace(Integer maxplace) {
        this.maxplace = maxplace;
    }
}
