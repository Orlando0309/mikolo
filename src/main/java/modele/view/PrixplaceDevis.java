package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("prixplacedevis")
public class PrixplaceDevis {
    @ColumnName
    
            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
            @LoadData(value = "service.ServiceCategoriePlace.list",datanamekey = "id",datalabelkey = "nom")
    Integer categorieplace;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "arrayOption")
            @LoadData(value = "service.ServiceDevis.list",datanamekey = "id",datalabelkey = "nomevenement")
    Integer devis;


    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getCategorieplace() {
        return categorieplace;
    }

    public void setCategorieplace(Integer categorieplace) {
        this.categorieplace = categorieplace;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getDevis() {
        return devis;
    }

    public void setDevis(Integer devis) {
        this.devis = devis;
    }
}
