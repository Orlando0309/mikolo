package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
import com.strawberry.annotation.Transient;
import constante.ConstanteEvent;
import java.io.IOException;
import java.util.Base64;
import org.springframework.web.multipart.MultipartFile;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("lieux")
public class Lieux {
    @ColumnName(nullable = true)
    
//            @FieldLoad(type = "number")
    Double prix;

    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer typeabn=ConstanteEvent.STANDARD;

    @ColumnName(insertable = false, hasdefault = true)
    
//            @FieldLoad(type = "number")
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer place;

    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer prestation=ConstanteEvent.LIEUX;

    @ColumnName
    
//            @FieldLoad(type = "number")
    Integer typetarif=1;

    @ColumnName
    
            @FieldLoad(type = "text")
    String nomelement;
    
    @Transient
    @LoadData(value = "service.ServiceTypelieu.list",datanamekey = "id",datalabelkey = "nomtypelieu")    
            @FieldLoad(type = "arrayOption")
    Integer typelieu;

    @FieldLoad(type = "file")
    @ColumnName("photo")
    String file;

    public Integer getTypelieu() {
        return typelieu;
    }

    public void setTypelieu(Integer typelieu) {
      
        this.typelieu = typelieu;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    
    

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
    public Integer getTypeabn() {
        return typeabn;
    }

    public void setTypeabn(Integer typeabn) {
        this.typeabn = typeabn;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }
    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }
    public Integer getTypetarif() {
        return typetarif;
    }

    public void setTypetarif(Integer typetarif) {
        this.typetarif = typetarif;
    }
    public String getNomelement() {
        return nomelement;
    }

    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }
}
