package modele.view;

import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
import constante.ConstanteEvent;
import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;
import scaffold.annotation.Show;

@TableName("elementprestation")
@Show(values = {"prix", "id", "nomelement","file"})
public class Artiste {

    @ColumnName

    @FieldLoad(type = "number")
    Double prix;

    @ColumnName

//            @FieldLoad(type = "arrayOption")
    Integer typeabn = 1;

    @Id
    @ColumnName(insertable = false, hasdefault = true)

    Integer id;

    @ColumnName

//            @FieldLoad(type = "arrayOption")
    Integer prestation = ConstanteEvent.ARTISTE;

    @ColumnName

    @FieldLoad(type = "arrayOption")
    @LoadData(value = "service.ServiceTypetarif.list", datalabelkey = "nomtarif", datanamekey = "id")
    Integer typetarif;

    @ColumnName

    @FieldLoad(type = "text")
    String nomelement;
    @ColumnName(nullable = true)

//            @FieldLoad(type = "number")
    Integer place;
    @FieldLoad(type = "file")
    @ColumnName("photo")
    String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
    

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public Integer getTypeabn() {
        return typeabn;
    }

    public void setTypeabn(Integer typeabn) {
        this.typeabn = typeabn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPrestation() {
        return prestation;
    }

    public void setPrestation(Integer prestation) {
        this.prestation = prestation;
    }

    public Integer getTypetarif() {
        return typetarif;
    }

    public void setTypetarif(Integer typetarif) {
        this.typetarif = typetarif;
    }

    public String getNomelement() {
        return nomelement;
    }

    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }
}
