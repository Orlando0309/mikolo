package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("v_nbplace")
public class V_nbplace {
    @ColumnName
    
            @FieldLoad(type = "number")
    Integer nombretotal;

    @ColumnName
    
            @FieldLoad(type = "number")
    Integer id;


    public Integer getNombretotal() {
        return nombretotal;
    }

    public void setNombretotal(Integer nombretotal) {
        this.nombretotal = nombretotal;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
