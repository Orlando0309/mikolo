package modele.view;
import com.strawberry.annotation.ColumnName;
import com.strawberry.annotation.Id;
import com.strawberry.annotation.TableName;
  import scaffold.annotation.FieldLoad;
import scaffold.annotation.LoadData;


@TableName("taxe")
public class Taxe {
    @ColumnName
    
            @FieldLoad(type = "number")
    Double pourcentage;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double maxvalue;

    @Id
    @ColumnName(insertable = false, hasdefault = true)
    
    Integer id;

    @ColumnName
    
            @FieldLoad(type = "number")
    Double minvalue;


    public Double getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(Double pourcentage) {
        this.pourcentage = pourcentage;
    }
    public Double getMaxvalue() {
        return maxvalue;
    }

    public void setMaxvalue(Double maxvalue) {
        this.maxvalue = maxvalue;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Double getMinvalue() {
        return minvalue;
    }

    public void setMinvalue(Double minvalue) {
        this.minvalue = minvalue;
    }
}
