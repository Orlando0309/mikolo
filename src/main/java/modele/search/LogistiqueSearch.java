package modele.search;

import modele.view.Logistique;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class LogistiqueSearch  extends SearchTools<Logistique> {
    
    public LogistiqueSearch(Class<Logistique> clT) {
        super(clT);
    }
    
}