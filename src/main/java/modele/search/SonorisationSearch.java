package modele.search;

import modele.view.Sonorisation;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class SonorisationSearch  extends SearchTools<Sonorisation> {
    
    public SonorisationSearch(Class<Sonorisation> clT) {
        super(clT);
    }
    
}