package modele.search;

import modele.view.V_prixdevis;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_prixdevisSearch  extends SearchTools<V_prixdevis> {
    
    public V_prixdevisSearch(Class<V_prixdevis> clT) {
        super(clT);
    }
    
}