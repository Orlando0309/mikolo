package modele.search;

import modele.view.Transport;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class TransportSearch  extends SearchTools<Transport> {
    
    public TransportSearch(Class<Transport> clT) {
        super(clT);
    }
    
}