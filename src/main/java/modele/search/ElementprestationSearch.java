package modele.search;

import modele.view.Elementprestation;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ElementprestationSearch  extends SearchTools<Elementprestation> {
    
    public ElementprestationSearch(Class<Elementprestation> clT) {
        super(clT);
    }
    
}