package modele.search;

import modele.view.Lieux;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class LieuxSearch  extends SearchTools<Lieux> {
    
    public LieuxSearch(Class<Lieux> clT) {
        super(clT);
    }
    
}