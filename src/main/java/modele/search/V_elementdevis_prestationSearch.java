package modele.search;

import modele.view.V_elementdevis_prestation;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_elementdevis_prestationSearch  extends SearchTools<V_elementdevis_prestation> {
    
    public V_elementdevis_prestationSearch(Class<V_elementdevis_prestation> clT) {
        super(clT);
    }
    
}