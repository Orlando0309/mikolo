package modele.search;

import modele.view.Employe;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class EmployeSearch  extends SearchTools<Employe> {
    
    public EmployeSearch(Class<Employe> clT) {
        super(clT);
    }
    
}