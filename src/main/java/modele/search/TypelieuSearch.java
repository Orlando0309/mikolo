package modele.search;

import modele.view.Typelieu;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class TypelieuSearch  extends SearchTools<Typelieu> {
    
    public TypelieuSearch(Class<Typelieu> clT) {
        super(clT);
    }
    
}