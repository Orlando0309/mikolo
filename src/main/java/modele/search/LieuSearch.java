package modele.search;

import modele.view.Lieu;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class LieuSearch  extends SearchTools<Lieu> {
    
    public LieuSearch(Class<Lieu> clT) {
        super(clT);
    }
    
}