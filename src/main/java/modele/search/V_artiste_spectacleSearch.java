package modele.search;

import modele.view.V_artiste_spectacle;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_artiste_spectacleSearch  extends SearchTools<V_artiste_spectacle> {
    
    public V_artiste_spectacleSearch(Class<V_artiste_spectacle> clT) {
        super(clT);
    }
    
}