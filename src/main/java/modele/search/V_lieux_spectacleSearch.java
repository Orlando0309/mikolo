package modele.search;

import modele.view.V_lieux_spectacle;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_lieux_spectacleSearch  extends SearchTools<V_lieux_spectacle> {
    
    public V_lieux_spectacleSearch(Class<V_lieux_spectacle> clT) {
        super(clT);
    }
    
}