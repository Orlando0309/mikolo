package modele.search;

import modele.view.CategoriePlace;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class CategoriePlaceSearch  extends SearchTools<CategoriePlace> {
    
    public CategoriePlaceSearch(Class<CategoriePlace> clT) {
        super(clT);
    }
    
}