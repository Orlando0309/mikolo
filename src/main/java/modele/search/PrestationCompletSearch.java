package modele.search;

import modele.view.PrestationComplet;
import scaffold.annotation.Search;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class PrestationCompletSearch  extends SearchTools<PrestationComplet> {
    @Search
    Integer prestation_id;
    public PrestationCompletSearch(Class<PrestationComplet> clT) {
        super(clT);
    }

    public Integer getPrestation_id() {
        return prestation_id;
    }

    public void setPrestation_id(Integer prestation_id) {
        this.prestation_id = prestation_id;
    }
    
    
    
    
}