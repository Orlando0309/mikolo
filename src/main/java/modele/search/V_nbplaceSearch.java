package modele.search;

import modele.view.V_nbplace;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_nbplaceSearch  extends SearchTools<V_nbplace> {
    
    public V_nbplaceSearch(Class<V_nbplace> clT) {
        super(clT);
    }
    
}