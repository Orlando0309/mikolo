package modele.search;

import modele.view.Devis;
import scaffold.annotation.FieldLoad;
import scaffold.annotation.Search;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class DevisSearch  extends SearchTools<Devis> {
    @Search(value = "like")
    @FieldLoad(type = "text")
    String nomevenement;
    

    public String getNomevenement() {
        return nomevenement;
    }

    public void setNomevenement(String nomevenement) {
        this.nomevenement = nomevenement;
    }
    
    public DevisSearch() {
        super(Devis.class);
    }
    
}