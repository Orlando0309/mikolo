package modele.search;

import modele.view.Elementdevis;
import scaffold.annotation.FieldLoad;
import scaffold.annotation.Search;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ElementdevisSearch  extends SearchTools<Elementdevis> {
    @Search(value = "minmax")
    @FieldLoad(type = "number")
    Double[] prix;

    public Double[] getPrix() {
        return prix;
    }
    
    
    

    public void setPrix(Double[] prix) {
        this.prix = prix;
    }
    
    public ElementdevisSearch(Class<Elementdevis> clT) {
        super(clT);
    }
    
}