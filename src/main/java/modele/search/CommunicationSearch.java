package modele.search;

import modele.view.Communication;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class CommunicationSearch  extends SearchTools<Communication> {
    
    public CommunicationSearch(Class<Communication> clT) {
        super(clT);
    }
    
}