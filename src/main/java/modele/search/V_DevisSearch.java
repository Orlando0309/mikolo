package modele.search;

import modele.view.V_Devis;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_DevisSearch  extends SearchTools<V_Devis> {
    
    public V_DevisSearch(Class<V_Devis> clT) {
        super(clT);
    }
    
}