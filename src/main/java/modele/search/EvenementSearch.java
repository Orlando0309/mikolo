package modele.search;

import modele.view.Evenement;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class EvenementSearch  extends SearchTools<Evenement> {
    
    public EvenementSearch(Class<Evenement> clT) {
        super(clT);
    }
    
}