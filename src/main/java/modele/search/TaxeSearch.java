package modele.search;

import modele.view.Taxe;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class TaxeSearch  extends SearchTools<Taxe> {
    
    public TaxeSearch(Class<Taxe> clT) {
        super(clT);
    }
    
}