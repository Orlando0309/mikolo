package modele.search;

import modele.view.PrixplaceDevis;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class PrixplaceDevisSearch  extends SearchTools<PrixplaceDevis> {
    
    public PrixplaceDevisSearch(Class<PrixplaceDevis> clT) {
        super(clT);
    }
    
}