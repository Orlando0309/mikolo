package modele.search;

import modele.view.Placevendu;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class PlacevenduSearch  extends SearchTools<Placevendu> {
    
    public PlacevenduSearch(Class<Placevendu> clT) {
        super(clT);
    }
    
}