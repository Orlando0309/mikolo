package modele.search;

import modele.view.Prestation;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class PrestationSearch  extends SearchTools<Prestation> {
    
    public PrestationSearch(Class<Prestation> clT) {
        super(clT);
    }
    
}