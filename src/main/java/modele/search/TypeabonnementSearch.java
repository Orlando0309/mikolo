package modele.search;

import modele.view.Typeabonnement;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class TypeabonnementSearch  extends SearchTools<Typeabonnement> {
    
    public TypeabonnementSearch(Class<Typeabonnement> clT) {
        super(clT);
    }
    
}