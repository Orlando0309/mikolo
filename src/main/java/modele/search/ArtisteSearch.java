package modele.search;

import modele.view.Artiste;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ArtisteSearch  extends SearchTools<Artiste> {
    
    public ArtisteSearch(Class<Artiste> clT) {
        super(clT);
    }
    
}