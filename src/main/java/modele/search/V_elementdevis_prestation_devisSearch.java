package modele.search;

import modele.view.V_elementdevis_prestation_devis;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class V_elementdevis_prestation_devisSearch  extends SearchTools<V_elementdevis_prestation_devis> {
    
    public V_elementdevis_prestation_devisSearch(Class<V_elementdevis_prestation_devis> clT) {
        super(clT);
    }
    
}