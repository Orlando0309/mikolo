package modele.search;

import modele.view.Typetarif;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class TypetarifSearch  extends SearchTools<Typetarif> {
    
    public TypetarifSearch(Class<Typetarif> clT) {
        super(clT);
    }
    
}