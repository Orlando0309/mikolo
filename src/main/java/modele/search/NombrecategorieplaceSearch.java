package modele.search;

import modele.view.Nombrecategorieplace;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class NombrecategorieplaceSearch  extends SearchTools<Nombrecategorieplace> {
    
    public NombrecategorieplaceSearch(Class<Nombrecategorieplace> clT) {
        super(clT);
    }
    
}