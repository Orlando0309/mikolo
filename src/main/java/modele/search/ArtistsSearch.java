package modele.search;

import modele.view.Artists;
import scaffold.search.SearchTools;

/**
 *
 * @author andri
 */
public class ArtistsSearch  extends SearchTools<Artists> {
    
    public ArtistsSearch(Class<Artists> clT) {
        super(clT);
    }
    
}